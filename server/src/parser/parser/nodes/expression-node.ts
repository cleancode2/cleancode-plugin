import { Range } from "vscode-languageserver";
import { ExpressionPartNode, Node } from "./_index";

export class ExpressionNode extends Node {
  private _parts: Array<ExpressionPartNode>;

  constructor(parts: Array<ExpressionPartNode>, range: Range) {
    super(range);
    this._parts = parts;
  }

  get parts(): Array<ExpressionPartNode> {
    return this._parts;
  }
}
