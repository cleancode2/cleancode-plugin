import { Range, uinteger } from "vscode-languageserver";
import { TextDocument } from "vscode-languageserver-textdocument";
import { RangeHelper } from "./range-helper";

export class Reader {
  private text?: TextDocument;
  private line: number;
  private char: number;
  private rangeHelper: RangeHelper;

  constructor() {
    this.line = 0;
    this.char = 0;
    this.rangeHelper = new RangeHelper();
  }

  setText(text: TextDocument): void {
    this.text = text;
  }

  getCurrent(): string {
    const lineRange = Range.create(this.line, 0, this.line, uinteger.MAX_VALUE);
    return this.text?.getText(lineRange).charAt(this.char) || "";
  }

  next(): void {
    if (this.text) {
      const lineRange = Range.create(
        this.line,
        0,
        this.line,
        uinteger.MAX_VALUE
      );
      if (this.char < this.text.getText(lineRange).length - 1) {
        this.char++;
      } else {
        this.char = 0;
        this.line++;
      }
    }
  }

  skipBlanks(): void {
    while (!this.isEnd() && this.getCurrent() === " ") {
      this.next();
    }
  }

  isEnd(): boolean {
    if (this.text) {
      const lineRange = Range.create(
        this.line,
        0,
        this.line,
        uinteger.MAX_VALUE
      );
      return (
        this.line === this.text.lineCount - 1 &&
        (this.char === this.text.getText(lineRange).length - 1 ||
          this.text.getText(lineRange).length === 0)
      );
    } else {
      return true;
    }
  }

  setStartIndex(): void {
    this.rangeHelper.setStart(this.line, this.line, this.char, this.char);
  }

  setEndIndex(): void {
    this.rangeHelper.setEnd(this.line, this.char);
  }

  getRange(): Range {
    return this.rangeHelper.range;
  }
}
