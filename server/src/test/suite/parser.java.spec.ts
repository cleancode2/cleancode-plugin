import { TextDocument } from "vscode-languageserver-textdocument";
import { JavaParser } from "../../parser/parser/implementations/java/java-parser";
import { ExpressionPartType, Node } from "../../parser/parser/nodes/_index";
import { AssertionHelper } from "../helper/assertion-helper";
import { CleanCodeVisitor } from "../../parser/parser/implementations/java/clean-code-visitor";
import chai from "chai";
import {
  methodDeclaratorCtx,
  normalClassDeclarationCtx,
  primaryCtx,
  variableDeclaratorIdCtx,
} from "../test-data/chevrotain-nodes";
import {
  MethodDeclaratorCtx,
  NormalClassDeclarationCtx,
  PrimaryCtx,
  VariableDeclaratorIdCtx,
} from "java-parser";
chai.should();

suite("Parser Test Suite", () => {
  suite("JavaParser", () => {
    test("unparsable document", () => {
      const document: TextDocument = getTextDocument("java", "class} A { in");
      const javaParser: JavaParser = new JavaParser();
      (function () {
        const nodes: Array<Node> = javaParser.parse(document);
        nodes.should.be.empty;
      }.should.not.throw(Error));
    });

    test("document with no nodes", () => {
      const document: TextDocument = getTextDocument("java", "");
      const javaParser: JavaParser = new JavaParser();
      const nodes: Array<Node> = javaParser.parse(document);
      nodes.should.be.empty;
    });

    test("document with class node", () => {
      const document: TextDocument = getTextDocument("java", "class A { }");
      const javaParser: JavaParser = new JavaParser();
      const nodes: Array<Node> = javaParser.parse(document);
      nodes.length.should.equal(1);
      AssertionHelper.assertClassNode(nodes[0], "A", 0, 6, 0, 7);
    });

    test("document with method node", () => {
      const document: TextDocument = getTextDocument(
        "java",
        "class A { void test() { } }"
      );
      const javaParser: JavaParser = new JavaParser();
      const nodes: Array<Node> = javaParser.parse(document);
      nodes.length.should.equal(2);
      AssertionHelper.assertClassNode(nodes[0], "A", 0, 6, 0, 7);
      AssertionHelper.assertMethodNode(nodes[1], 0, "test", 0, 15, 0, 19);
    });

    test("document with variable node", () => {
      const document: TextDocument = getTextDocument(
        "java",
        "class A { int x = 10; }"
      );
      const javaParser: JavaParser = new JavaParser();
      const nodes: Array<Node> = javaParser.parse(document);
      nodes.length.should.equal(2);
      AssertionHelper.assertClassNode(nodes[0], "A", 0, 6, 0, 7);
      AssertionHelper.assertVariableNode(nodes[1], "x", 0, 14, 0, 15);
    });

    test("document with multiple nodes", () => {
      const document: TextDocument = getTextDocument(
        "java",
        "class A {\n" +
          "  int x = 10;\n" +
          "  int y = 15;\n" +
          "  void test() { }\n" +
          "  int add() {\n" +
          "    return x + y;\n" +
          "  }\n" +
          "  void set(int newX) {\n" +
          "    x = newX;\n" +
          "  }\n" +
          "}"
      );
      const javaParser: JavaParser = new JavaParser();
      const nodes: Array<Node> = javaParser.parse(document);
      nodes.length.should.equal(7);
      AssertionHelper.assertClassNode(nodes[0], "A", 0, 6, 0, 7);
      AssertionHelper.assertVariableNode(nodes[1], "x", 1, 6, 1, 7);
      AssertionHelper.assertVariableNode(nodes[2], "y", 2, 6, 2, 7);
      AssertionHelper.assertMethodNode(nodes[3], 0, "test", 3, 7, 3, 11);
      AssertionHelper.assertMethodNode(nodes[4], 0, "add", 4, 6, 4, 9);
      AssertionHelper.assertMethodNode(nodes[5], 1, "set", 7, 7, 7, 10);
      AssertionHelper.assertVariableNode(nodes[6], "newX", 7, 15, 7, 19);
    });
  });

  suite("CleanCodeVisitor", () => {
    test("initialization", () => {
      const cleanCodeVisitor: CleanCodeVisitor = new CleanCodeVisitor();
      cleanCodeVisitor.nodes.should.be.empty;
    });

    test("normalClassDeclaration", () => {
      const normalClassDeclaration: NormalClassDeclarationCtx =
        normalClassDeclarationCtx;
      const cleanCodeVisitor: CleanCodeVisitor = new CleanCodeVisitor();
      cleanCodeVisitor.normalClassDeclaration(normalClassDeclaration);
      AssertionHelper.assertClassNode(
        cleanCodeVisitor.nodes[0],
        "A",
        0,
        13,
        0,
        14
      );
    });

    test("variableDeclaratorId", () => {
      const variableDeclaratorId: VariableDeclaratorIdCtx =
        variableDeclaratorIdCtx;
      const cleanCodeVisitor: CleanCodeVisitor = new CleanCodeVisitor();
      cleanCodeVisitor.variableDeclaratorId(variableDeclaratorId);
      AssertionHelper.assertVariableNode(
        cleanCodeVisitor.nodes[0],
        "x",
        0,
        21,
        0,
        22
      );
    });

    test("methodDeclarator", () => {
      const methodDeclarator: MethodDeclaratorCtx = methodDeclaratorCtx;
      const cleanCodeVisitor: CleanCodeVisitor = new CleanCodeVisitor();
      cleanCodeVisitor.methodDeclarator(methodDeclarator);
      AssertionHelper.assertMethodNode(
        cleanCodeVisitor.nodes[0],
        0,
        "test",
        0,
        34,
        0,
        38
      );
    });

    test("primary", () => {
      const primary: PrimaryCtx = primaryCtx;
      const cleanCodeVisitor: CleanCodeVisitor = new CleanCodeVisitor();
      cleanCodeVisitor.primary(primary);
      AssertionHelper.assertExpressionNode(
        cleanCodeVisitor.nodes[0],
        [
          ExpressionPartType.fieldCall,
          ExpressionPartType.methodCall,
          ExpressionPartType.methodCall,
          ExpressionPartType.fieldCall,
        ],
        ["foo", "bar", "test", "foobar"],
        -1,
        -1,
        -1,
        25
      );
    });
  });
});

function getTextDocument(languageId: string, content: string): TextDocument {
  return TextDocument.create("file://test", languageId, 1, content);
}
