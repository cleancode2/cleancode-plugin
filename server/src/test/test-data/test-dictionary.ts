import { Dictionary } from "../../resources/dictionary";

export const dictionary: Dictionary = {
  dictionary: [
    {
      word: "aardvark",
      definitions: [
        {
          definition:
            "nocturnal burrowing mammal of the grasslands of Africa that feeds on termites; sole extant representative of the order Tubulidentata",
          partOfSpeech: "noun",
        },
      ],
    },
    {
      word: "aardvarks",
      definitions: [
        {
          definition:
            "nocturnal burrowing mammal of the grasslands of Africa that feeds on termites; sole extant representative of the order Tubulidentata",
          partOfSpeech: "noun",
        },
      ],
    },
    {
      word: "are",
      definitions: [
        {
          definition: "a unit of surface area equal to 100 square meters",
          partOfSpeech: "noun",
        },
      ],
    },
    {
      word: "mammal",
      definitions: [
        {
          definition:
            "any warm-blooded vertebrate having the skin more or less covered with hair; young are born alive except for the small subclass of monotremes and nourished with milk",
          partOfSpeech: "noun",
        },
      ],
    },
    {
      word: "mammals",
      definitions: [
        {
          definition:
            "any warm-blooded vertebrate having the skin more or less covered with hair; young are born alive except for the small subclass of monotremes and nourished with milk",
          partOfSpeech: "noun",
        },
      ],
    },
    {
      word: "argufy",
      definitions: [
        {
          definition: "have a disagreement over something",
          partOfSpeech: "verb",
        },
      ],
    },
  ],
};
