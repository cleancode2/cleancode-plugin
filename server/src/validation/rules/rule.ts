import { CleanCodeConfig } from "../../clean-code-config";
import { Node } from "../../parser/parser/nodes/_index";

export interface Rule {
  checkRule(node: Node, settings: CleanCodeConfig): void;
}
