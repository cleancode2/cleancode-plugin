import { Range } from "vscode-languageserver";
import { Node } from "./_index";

export abstract class IdentifierNode extends Node {
  private _name: string;

  constructor(name: string, range: Range) {
    super(range);
    this._name = name;
  }

  get name(): string {
    return this._name;
  }
}
