# Law Of Demeter

## Zusammenfassung

Das Law Of Demeter ist eine Regel aus der Objektorientierten Softwareentwicklung. Sie besagt, dass Objekte nur mit anderen Objekten aus ihrer unmittelbaren Umgebung kommunizieren sollen. Als Analogie wird oft der Grundsatz "Sprich nur zu deinen nächsten Freunden, nicht zu Fremden" angeführt. Diese Regel soll dafür sorgen, dass die Kopplung innerhalb der Software reduziert und damit die Wartbarkeit erhöht wird.

Genauer formuliert besagt das Law Of Demeter, dass eine Methode f, einer Klasse K nur Methoden der folgenden Objekte aufrufen solle:

- K
- Objekte die von f erstellt wurden
- Objekte welche als Argument an f übergeben werden
- Objekte die in einer Instanzvariablen von K enthalten sind

Auf Objekten die von den erlaubten Methoden zurückgegeben werden, sollen keine Methoden aufgerufen werden.
  
Wenn Verstösse gegen das Law Of Demeter festgestellt werden, können diese mittels Kapselung von Objekten in einem Refactoring meistens behoben werden.

### Problembeschreibung

Wenn das Law of Demeter nicht eingehalten wird, kann es schnell zu Verwirrung kommen. Diese Verwirrung kann zu hybriden Strukturen führen, welche halb Objekt und halb Datenstruktur sind und sich dadurch auszeichenen, dass sie sowohl Funktionen anbieten, die wichtige Aufgaben erfüllen und gleichzeitig öffentliche Variablen bzw. Mutatoren haben die die privaten Variablen enthüllen. Dies kann dann externe Funktionen dazu verleiten, diese Variablen so zu verwenden, wie dies ein prozedurales Programm tun würde, was nicht dem Grundgedanken der Objektorientierung entspricht.

Die Verletzung des Law of Demeter bedeutet ausserdem automatisch eine erhöhte Koppelung innerhalb der Softwarekomponenten, was die Wartbarkeit der Software verschlechtert.

Beispiel:

    var address = account.getAccountHolder().getAddress();

Im gegeben Beispiel wird das Law Of Demeter verletzt, da zuerst die Methode getAccountholder() vom Objekt Account aufgerufen wird, welche einen Accountholder Objekt zurückgibt. Auf diesem Objekt wird dann wieder eine eigene Methode (getAddress()) aufgerufen. Es wird also mit einem "Fremden" gesprochen, da nur Account als Objekt aus unmittelbarer Umgebung angesehen werden kann.

## Lösung

Als Lösung gegen Verstösse des Law Of Demeter kann Kapselung eingesetzt werden. Dafür werden Wrapper Methoden implementiert welche den Aufruf and die Fremde Klasse delegieren.

Beispiel:

    class Motor {
        public void start() {
            // start the motor
        }
    }

    class Car {
        private Motor motor;
        public Car() {
            motor = new Motor();
        }
        public Motor getMotor() {
            return motor;
        }
    }
    class Driver {
        public void drive() {
            Car car = new Car();
            car.getMotor().start(); // Violation of Law of Demeter
        }
    }

Um den Verstoss gegen das Law Of Demeter zu beseitigen, würde hier eine Wrapper Methode in der Klasse Car hinzugefügt werden, welche den Aufruf an die Motor Klasse delegiert.

    class Motor {
        public void start() {
           // start the motor
       }
    }
    class Car {
        private Motor motor;
        public Car() {
            motor = new Motor();
        }
        public void readyCar() {
            motor.start();
        }
    }
    class Driver {
        public void drive() {
            Car car = new Car();
            car.readyCar();
        }
    }

Daraus entsteht nun der Vorteil, das die readyCar() Methode beliebig modifiziert werden kann, ohne das der Aufrufende weitere Details zur Methode kennen muss.

## Code Beispiele

### Positive Beispiele

    public class LawOfDemeter{
        private Topping cheeseTopping;
  
        public void goodExamples(Pizza pizza){
            Foo foo = new Foo();
    
            // (1) it's okay to call our own methods
            doSomething();
    
            // (2) it's okay to call methods on objects passed in to our method
            int price = pizza.getPrice();
    
            // (3) it's okay to call methods on any objects we create
            cheeseTopping = new CheeseTopping();
            float weight = cheeseTopping.getWeightUsed();
    
            // (4) any directly held component objects
            foo.doBar();
        }
  
        private void doSomething()
        {
            // do something here ...
        }
    }

### Negative Beispiele

    var data = new A().GetObjectB().GetObjectC().GetData();
    
    customer.getMailingAddress().getLine1();
  
    employee.getHomeAddress().getLine1();

## Vorteile der Regel

- Kopplung wird verringert
- Wartbarkeit wird erhöht
- Klassen können einfacher wiederverwendet werden
- Die Testbarkeit wird verbessert
- Klassen die dem Law Of Demeter folgen weisen seltener fehlerhaftes Verhalten auf

## Nachteile der Regel

- Initial grösserer Programmieraufwand
- Minimaler negativer Impact auf die Performance und leicht höherer Arbeitsspeicherverbrauch zur Laufzeit

## FAQ

### Bei der Verwendung der Java Stream API wird mir ein Verstoss gegen das Law Of Demeter angezeigt. Heisst das, dass ich die Stream API für sauberen Code nicht verwenden darf?

Antwort: Nein, das heisst es nicht. Mit der Java Stream API lassen sich Elemente in einem funktionalen Programmierstil manipulieren. Da es sich um funktionale Ansätze handelt, verstösst dies zwar gegen Objektorientierte Grundsätze wie dem Law Of Demeter, dies heisst aber nicht automatisch, dass der Code dadurch schlecht ist. Die Stream API ist ein von Java bereitgestelltes Tool der Programmiersprache und kann daher problemlos verwendet werden.

Wir werden in der Zukunft eine Einstellung bereitstellen, bei der man Meldungen über Verstösse des Law Of Demeters bei der Verwendung der Stream API deaktivieren kann.
