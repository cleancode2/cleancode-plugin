import { CleanCodeConfig } from "../../clean-code-config";
import { ClassNode, Node } from "../../parser/parser/nodes/_index";
import { Dictionary } from "../../resources/dictionary";
import { WordType } from "../helper/identifier-helper";
import { IdentifierNameRule } from "../rules/_index";
import { Validator } from "./validator";

export class ClassNameValidator implements Validator {
  private identifierRule: IdentifierNameRule;

  constructor(dictionary: Dictionary) {
    this.identifierRule = new IdentifierNameRule(dictionary);
  }

  validate(nodes: Array<Node>, settings: CleanCodeConfig): void {
    nodes.forEach((node) => {
      if (node instanceof ClassNode) {
        this.identifierRule.checkRule(node, settings, WordType.noun, "class");
      }
    });
  }
}
