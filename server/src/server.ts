import {
  createConnection,
  TextDocuments,
  Diagnostic,
  ProposedFeatures,
  TextDocumentSyncKind,
  InitializeResult,
  DidChangeConfigurationNotification,
} from "vscode-languageserver/node";
import { TextDocument } from "vscode-languageserver-textdocument";
import { ParseManager } from "./parser/parse-manager";
import { ValidationManager } from "./validation/validation-manager";
import { NotificationManager } from "./notification/notification-manager";
import { CleanCodeConfig, defaultConfig } from "./clean-code-config";
import dictionary from "./resources/dictionary.json";

const connection = createConnection(ProposedFeatures.all);
const documents: TextDocuments<TextDocument> = new TextDocuments(TextDocument);

let configFile: CleanCodeConfig | undefined;
let workspaceSettings: CleanCodeConfig;
let diagnostics: Diagnostic[];

connection.onInitialize(async () => {
  const result: InitializeResult = {
    capabilities: {
      textDocumentSync: TextDocumentSyncKind.Incremental,
    },
  };

  return result;
});

connection.onInitialized(() => {
  connection.client.register(
    DidChangeConfigurationNotification.type,
    undefined
  );
});

async function getCleanCodeConfig(): Promise<CleanCodeConfig> {
  await loadCleanCodeConfig();
  return configFile ?? workspaceSettings ?? defaultConfig;
}

async function loadCleanCodeConfig() {
  if (!configFile) {
    const body = await connection.sendRequest<string>(
      "getFile",
      "cleancodeconfig.json"
    );

    if (body && body.length) {
      try {
        configFile = JSON.parse(body);
      } catch {
        connection.window.showWarningMessage(
          "cleancodeconfig.json could not be parsed"
        );
      }
    }
  }

  if (!workspaceSettings) {
    const config = await connection.workspace.getConfiguration({
      scopeUri: undefined,
      section: "cc-language-server",
    });

    if (config) {
      workspaceSettings = config;
    }
  }
}

connection.onDidChangeConfiguration((change) => {
  workspaceSettings = <CleanCodeConfig>change?.settings.languageserver;
  documents.all().forEach(validateTextDocument);
});

documents.onDidChangeContent((change) => {
  validateTextDocument(change.document);
});

connection.onDidChangeWatchedFiles(() => {
  configFile = undefined;
  documents.all().forEach(validateTextDocument);
});

async function validateTextDocument(textDocument: TextDocument): Promise<void> {
  const settings = await getCleanCodeConfig();

  const parseManager = new ParseManager(textDocument.languageId);
  const nodes = parseManager.parseDocument(textDocument);
  const validationManager = new ValidationManager(dictionary);
  const ruleViolations = validationManager.validateRules(nodes, settings);
  if (nodes.length !== 0) {
    const notificationManager = new NotificationManager();
    diagnostics = notificationManager.getDiagnostics(ruleViolations, settings);
    connection.sendDiagnostics({ uri: textDocument.uri, diagnostics });
  }
}

documents.listen(connection);
connection.listen();
