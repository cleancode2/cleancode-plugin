import { CleanCodeConfig } from "../../clean-code-config";
import { Node } from "../../parser/parser/nodes/_index";

export interface Validator {
  validate(nodes: Array<Node>, settings?: CleanCodeConfig): void;
}
