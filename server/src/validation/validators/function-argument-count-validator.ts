import { CleanCodeConfig } from "../../clean-code-config";
import { MethodNode, Node } from "../../parser/parser/nodes/_index";
import { FunctionArgumentCountRule } from "../rules/function-argument-count-rule";
import { Validator } from "./validator";

export class FunctionArgumentCountValidator implements Validator {
  private rule: FunctionArgumentCountRule;

  constructor() {
    this.rule = new FunctionArgumentCountRule();
  }
  validate(nodes: Array<Node>, settings: CleanCodeConfig): void {
    nodes.forEach((node) => {
      if (node instanceof MethodNode) {
        this.rule.checkRule(node, settings);
      }
    });
  }
}
