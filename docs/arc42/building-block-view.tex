\hypertarget{section-building-block-view}{%
  \section{Building Block View}\label{section-building-block-view}}

The extension is split into two parts, a client and a server. While the client is relatively light weight, the server itself is split into several subsystems, which perform their parts of the code validation independantly. These subsystems are tied together by the server extension that then invokes whichever system is required for the next step. This design enables us to replace single subsystems if required. The client and server communicate through the Language Server Protocol over the Node inter-process communication module.

\hypertarget{whitebox-overall-system}{%
  \subsection{Whitebox Overall System}\label{whitebox-overall-system}}

\begin{figure}[h]
  \includegraphics{images/whitebox-overview.png}
  \caption{Overall System}\label{fig:whitebox-overview}
\end{figure}

\pagebreak

\begin{description}
  \item[Description]
    The extension is actually comprised of two extensions that communicate through the LSP~\footnote{https://microsoft.github.io/language-server-protocol/} over Node ipc. This allows offloading of computation-heavy operations to another process instead of blocking Visual Studio Code with long running functions.
  \item[Client]
    The client is a lightweigt extension that runs in the same process as Visual Studio Code and just informs the server what files are opened, closed and edited. It furthermore communicates initial states and changes in user settings relevant to the extension.
  \item[Server]
    The server is an implementation of a language server in Node. It runs in an independant process and could in theory be used as the language server for other plugins in other IDEs. The server runs the code checking logic and returns violations to the client.
  \item[Contained Building Blocks]
    The server has three main subsystems, the \texttt{ParseManager} to transform source code into \texttt{Nodes}, the \texttt{ValidationManager} to check if any of those \texttt{Nodes} contain violations against our rules and lastly the \texttt{NotificationManager} which converts the results into useful feedback for the developer.
  \item[Important Interfaces]
    All communication between the client and the server happens over the node ipc channel that is opened when the extension is initialized.
  \item[Important Concepts]
    Interfaces are used for objects that need language specific implementations (i.e.\ parser, lexer) as well as for objects that are implemented in different variations (i.e.\ rules, validators). One reason for this is to hide implementation details from calling classes. Another reason is the ability to decide at runtime which implementation should be used for a given situation.

    Abstract classes are used for classes that provide a set of basic funtionalities but are designed to be inherited and extended with specific functionality. An example for this is the class \texttt{Node} which provides the functionality to find its location in the source code but needs to be extended with the specific logic depending on the type of node.

    The get/set functionality of TypeScript is used instead of traditional accessor methods to make private fields available outside of a class.
    \begin{verbatim}
    class Token {
      private_range: vscode.Range;

      get range(): vscode.Range {
        return this._range;
      }

      set range(range: vscode.Range) {
        this._range = range;
      }
    }

    ...

    var token = new Token();
    token.range = new Range(1, 1);
  \end{verbatim}
\end{description}

\hypertarget{whitebox-client}{%
  \subsection{Client}\label{whitebox-client}}

The client extension does not contain any of the logic required for the rule validation. It is intentionally kept as simple as possible so that it is less work to implement a corresponding client plugin in a different IDE while reusing the server extension as is. The singular TypeScript file is \texttt{extension.ts} which implements the functions \texttt{activate} and \texttt{deactivate} which are used by Visual Studio Code to activate and deactivate the extension. In addition there are some functions to handle interaction with the context menu entries the extension adds to editor views of supported languages and to look for a configuration file and make it available to the server.

\hypertarget{whitebox-server}{%
  \subsection{Server}\label{whitebox-server}}

The server extension implements all of the logic for validating rules. It is comprised of dufferent subsystems to parse source code, validate rules and notify the developer. These subsystems don't rely on each other so that they can be reworked or replaced without the other subsystems needing any change.

\hypertarget{whitebox-parsemanager}{%
  \subsubsection{Parsing}\label{whitebox-parsemanager}}

An overview of the parsing subsystem can be found in figure~\ref{fig:whitebox-parsing}.

\begin{description}
  \item[Description]
    The logic for parsing source code is managed by the \texttt{ParseManager} class. It chooses the correct \texttt{Parser} for a given language which then extracts the required \texttt{Nodes} from the document.

    The \texttt{Parser} interface defines what functions a language specific parser has to implement. The actual implementation of a parser can vary. If there are robust libraries available for a given language, one of those can be used to do the heavy lifting. The parser then only has to map the return of the used library to corresponding \texttt{Nodes} so that it returns in a format that the following subsystems understand. If no usable library exists a parser can be implemented with the help of the existing \texttt{Lexer} interface and the associated helper classes.
  \item[Contained Building Blocks]
    The \texttt{JavaParser} class is an implementation of the \texttt{Parser} interface for the programming language Java. It uses the \texttt{java-parser} npm package which returns a concrete syntax tree for a given java document. The tree then is traversed using the \texttt{CleanCodeVisitor} during which the relevant \texttt{Nodes} are collected and returned to the \texttt{ParseManager} at the end.

    Specific \texttt{Nodes} that contain all relevant information required for the implemented rule validations.
  \item[Important Interfaces]
    The \texttt{parseDocument} method receives a document and returns a list of all relevant \texttt{Nodes}.
  \item[Important Concepts]
    The visitor pattern is used to traverse the syntax tree that is built by the used parsing library. The pattern enables visiting all leaves of a tree and executing operations on chosen leaves while not changing anything on the underlying tree class.
\end{description}

\begin{figure}[h]
  \includegraphics{images/whitebox-parsing.png}
  \caption{Overview Parsing Subsystem}\label{fig:whitebox-parsing}
\end{figure}

\hypertarget{whitebox-validationmanager}{%
  \subsubsection{Validation}\label{whitebox-validationmanager}}

An overview of the validation subsystem can be found in figure~\ref{fig:whitebox-validation} and figure~\ref{fig:whitebox-validation-validators}.

\begin{description}
  \item[Description]
    The class \texttt{ValidationManager} manages the validation of rules. For specific rules the interface \texttt{Rule} with its method \texttt{checkRule} is implemented. The implemented rules are provided to the \texttt{ValidationManager} through implementations of the \texttt{Validator} interface.

    The additional layer between \texttt{Rule} and \texttt{ValidationManager} improves flexibility, because it enables the reuse of rule logic for several \texttt{Nodes}, if they have to abide by the same rules.

    Rule infractions are added to the \texttt{ValidationManager} property \texttt{RuleViolations} which is returned after all the checks have run.
  \item[Contained Building Blocks]
    \texttt{Validator} implementations check a list of specific \texttt{Nodes} against set of rules defined for those nodes.

    \texttt{Rule} implementations check a single \texttt{Node} against a rule.

    The class \texttt{IdentifierHelper} implements logic to break an identifier into its individual words and to check if those words are contained in the dictionary.

    The \texttt{RuleViolation} class contains all the information about the position of a rule violation in the source code and about its type. This information is later used to notify the developer.
  \item[Important Interfaces]
    The \texttt{validateRules} method takes a list of \texttt{Nodes} and checks whether they violate the implemented rules.
\end{description}

\begin{figure}[h]
  \includegraphics{images/whitebox-validation.png}
  \caption{Overview Validation Subsystem}\label{fig:whitebox-validation}
\end{figure}

\begin{landscape}
  \begin{figure}[h]
    \includegraphics{images/whitebox-validation-validators.png}
    \caption{Validators and Rules}\label{fig:whitebox-validation-validators}
  \end{figure}
\end{landscape}

\hypertarget{whitebox-notificationmanager}{%
  \subsubsection{Notification}\label{whitebox-notificationmanager}}

\begin{description}
  \item[Description]
    The class \texttt{NotificationManager} translates the rule violations that were found into Visual Studio Code \texttt{Diagnostic} objects and returns them.

    Depending on the settings it returns them as either a warning, an error or not at all.
  \item[Important Interfaces]
    The \texttt{createDiagnostics} method takes a list of \texttt{RuleViolation}s and the current \texttt{CleanCodeConfig} and returns a list of \texttt{Diagnostic}s.
\end{description}
