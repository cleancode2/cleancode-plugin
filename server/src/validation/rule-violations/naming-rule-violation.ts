import { Node } from "../../parser/parser/nodes/_index";
import { RuleViolation } from "./_index";
import dotenv from 'dotenv';
import path from "path";

export class NamingRuleViolation extends RuleViolation {
  constructor(node: Node, identifierPart: string) {
    dotenv.config({ path: path.resolve(__dirname, '../../../../urls.env') });
    super(node, `${identifierPart} is not a word \nidentifiers should be comprised of full words. Learn more at ${process.env.WIKIPAGE_BASE_URL}${process.env.NAMING_RULE_URL}`);
  }
}
