import { MethodNode } from "../../parser/parser/nodes/_index";
import { RuleViolation } from "./_index";
import dotenv from 'dotenv';
import path from "path";

export class FunctionArgumentCountRuleViolation extends RuleViolation {
  constructor(node: MethodNode) {
    dotenv.config({ path: path.resolve(__dirname, '../../../../urls.env') });
    super(node, `${node.name} has too many arguments (${node.argumentCount})\nfunctions should not have more than 3 arguments. Learn more at ${process.env.WIKIPAGE_BASE_URL}${process.env.FUNCTION_ARGUMENT_RULE_URL}`);
  }
}
