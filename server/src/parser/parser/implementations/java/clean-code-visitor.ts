import { Range } from "vscode-languageserver";
import {
  BaseJavaCstVisitorWithDefaults,
  FqnOrRefTypeCstNode,
  FqnOrRefTypePartFirstCstNode,
  IToken,
  MethodDeclaratorCtx,
  NormalClassDeclarationCtx,
  PrimaryCtx,
  PrimaryPrefixCstNode,
  VariableDeclaratorIdCtx,
} from "java-parser";
import { CstNodeLocation } from "chevrotain";
import {
  ClassNode,
  ExpressionNode,
  ExpressionPartNode,
  MethodNode,
  Node,
  VariableNode,
} from "../../nodes/_index";
import { ExpressionPartType } from "../../nodes/expression-part-node";

export class CleanCodeVisitor extends BaseJavaCstVisitorWithDefaults {
  private _nodes: Array<Node>;

  constructor() {
    super();
    this._nodes = new Array<Node>();
    this.validateVisitor();
  }

  get nodes(): Array<Node> {
    return this._nodes;
  }

  normalClassDeclaration(context: NormalClassDeclarationCtx) {
    const identifier = context.typeIdentifier[0].children.Identifier[0];
    const range = this.getRangeFromToken(identifier);
    const node = new ClassNode(identifier.image, range);
    this._nodes.push(node);
    this.visit(context.classBody);
  }

  variableDeclaratorId(context: VariableDeclaratorIdCtx) {
    const identifier = context.Identifier[0];
    const range = this.getRangeFromToken(identifier);
    const node = new VariableNode(identifier.image, range);
    this._nodes.push(node);
  }

  methodDeclarator(context: MethodDeclaratorCtx) {
    const identifier = context.Identifier[0];
    const range = this.getRangeFromToken(identifier);
    let argumentCount = 0;

    if (context.formalParameterList) {
      argumentCount =
        context.formalParameterList[0].children.formalParameter.length;
    }
    const node = new MethodNode(identifier.image, argumentCount, range);
    this._nodes.push(node);
    if (context.formalParameterList) {
      this.visit(context.formalParameterList);
    }
  }

  primary(context: PrimaryCtx) {
    if (context.primaryPrefix[0]) {
      const prefix = context.primaryPrefix[0];
      if (prefix.children.fqnOrRefType) {
        const functionOrReferenceType = prefix.children.fqnOrRefType[0];
        const firstPart =
          functionOrReferenceType.children.fqnOrRefTypePartFirst[0];

        if (firstPart.children.fqnOrRefTypePartCommon[0].children.Identifier) {
          const parts: Array<ExpressionPartNode> = [];
          const previousPart: IToken | undefined = this.handlePrimarySuffix(functionOrReferenceType, firstPart, parts);
          this.handleSuffix(context, previousPart, parts, prefix);
        }
      }
    }
  }

  private handlePrimarySuffix(functionOrReferenceType: FqnOrRefTypeCstNode, firstPart: FqnOrRefTypePartFirstCstNode, parts: Array<ExpressionPartNode>): IToken | undefined {
    if (firstPart.children.fqnOrRefTypePartCommon[0].children.Identifier) {
      let previousPart: IToken | undefined =
        firstPart.children.fqnOrRefTypePartCommon[0].children.Identifier[0];

      const restPart =
        functionOrReferenceType.children.fqnOrRefTypePartRest;
      if (restPart) {
        for (const child of restPart) {
          if (
            child.children.fqnOrRefTypePartCommon[0].children.Identifier
          ) {
            this.addPart(previousPart, parts);
            previousPart =
              child.children.fqnOrRefTypePartCommon[0].children
                .Identifier[0];
          }
        }
      }

      return previousPart;
    }
  }

  private handleSuffix(context: PrimaryCtx, previousPart: IToken | undefined, parts: Array<ExpressionPartNode>, prefix: PrimaryPrefixCstNode) {
    const suffix = context.primarySuffix;
    if (suffix) {
      for (const child of suffix) {
        if (child.children.methodInvocationSuffix) {
          this.addPart(
            previousPart,
            parts,
            ExpressionPartType.methodCall
          );
          previousPart = undefined;
        } else if (child.children.Identifier) {
          this.addPart(previousPart, parts);
          previousPart = child.children.Identifier[0];
        }
      }

      this.addPart(previousPart, parts);

      const node = new ExpressionNode(
        parts,
        this.getRangeFromTwoLocations(
          prefix.location,
          suffix[suffix.length - 1].location
        )
      );
      this._nodes.push(node);
    }
  }

  private addPart(
    identifier: IToken | undefined,
    parts: Array<ExpressionPartNode>,
    type: ExpressionPartType = ExpressionPartType.fieldCall
  ) {
    if (identifier) {
      parts.push(
        new ExpressionPartNode(
          type,
          parts.length + 1,
          identifier.image,
          this.getRangeFromToken(identifier)
        )
      );
    }
  }

  private getRangeFromToken(token: IToken): Range {
    return {
      start: {
        line: token.startLine - 1,
        character: token.startColumn - 1,
      },
      end: { line: token.endLine - 1, character: token.endColumn },
    };
  }

  private getRangeFromTwoLocations(
    begin: CstNodeLocation,
    end: CstNodeLocation
  ): Range {
    return {
      start: {
        line: begin.startLine - 1,
        character: (begin.startColumn ?? 0) - 1,
      },
      end: {
        line: (end.endLine ?? 0) - 1,
        character: end.endColumn ?? -1,
      },
    };
  }
}
