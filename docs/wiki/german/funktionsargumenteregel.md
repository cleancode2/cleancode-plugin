# Anzahl Parameter einer Funktion

## Zusammenfassung

Die optimale Anzahl von Argumenten fur eine Funktion ist gemäss Robert C. Martin = 0. Auch 1 und 2 Argumente sind noch in Ordnung. Ein drittes Argument sollte wenn möglich vermieden werden. Mehr als drei Argumente sollten niemals benutzt werden. Begründet wird dies mit der grossen konzeptionellen Vorstellungskraft die zum Verständnis jedes zusätzlichen Arguments erforderlich ist. Ausserdem wird es mit der steigenden Anzahl von Argumenten immer schwieriger, die Funktion sinnvoll zu testen. Die Software sollte dementsprechend angepasst werden.

## Problembeschreibung

Das konzeptionelle Verständnis für Argumente ist für unser Gehirn schwer. Bereits ein Argument erhöht die Anforderungen für das konzeptionelle Verständnis (Vorstellungskraft) des menschlichen Gehirns im Vergleich zu keinem Argument enorm. Diese Schwierigkeit steigt mit zunehmender Anzahl Argumente exponentiell. Eine Funktion ohne Argumente lässt sich ebenfalls viel einfacher testen als eine Funktion mit einem Argument. Je mehr Argumente eine Funktion hat, desto grösser wird der Aufwand, eine Funktion sinnvoll und vollständig Testen zu können. Beide Tatsachen führen zu grösserem Zeitaufwand & geringerer Entwicklereffizienz und damit zu höheren Entwicklungskosten.

## Lösung

Nach Möglichkeit sollten Funktionen ohne Argumente verwendet werden.

In den Fällen, in denen dies nicht möglich ist, sollten Funktionen mit einem Argument verwendet werden. In spezielleren Fällen, ist es auch in Ordnung 2 oder max. 3 Argumente zu verwenden. Werden 3 oder mehr Argumente benötigt um den gewünschten Effekt zu realisieren, deutet das auf ein grössers Problem und man sollte über ein Refactoring dieses Teils der Software nachdenken.

Wenn eine Funktion 2 oder mehr Argumente benötigt um zu funktionieren, ist es wahrscheinlich, dass einige dieser Argumente in eine separate Klasse eingehüllt werden sollten:

### Beispiel

Bei der folgenden Funktion:

    void print_circle(double x, double y, double radius){...}

Kann für die beiden double Koordinaten x & y eine neue Klasse Point erstellt werden.

    class Point(double x, double y){...}

Wodurch die Anzahl der Argumente von 3 auf 2 gesenkt werden kann:

    void print_circle(Point point, double radius){..}


## Code Beispiele

### Positive Beispiele

Funktion mit 0 Argumenten:

    void print_hello(){
        print("hello");
    }

Funktion mit 1 Argument:

    void print_shape(shape){
        print(shape);
    }
    print(circle)

Funktion mit 2 Argumenten:

    void print_circle(Point point, double radius){..}

### Negatives Beispiel

Funktion mit 4 Argumenten:

    void print_triangle(double x, double y, double z, string color){...}

Hier können alle 4 Argumente in eine eigene Klasse eingehüllt werden:

    class Triangle(double x, double y, double z, string color){...}

Und damit wird die Anzahl der Funktionsargumente auf 1 reduziert:
    void print_triangle(Triangle triangle){...}


## Vorteile der Regel

Die folgenden Vorteile entstehen bei der Einhaltung dieser Regel:

- Code kann einfacher gelesen & verstanden werden
- Die Objektstrukturierung verbessert sich automatisch

## Nachteile der Regel

Die folgenden Nachteile entstehen bei der Einhaltung dieser Regel:

- Höherer initialer Aufwand beim Entwickeln
- Es ist schwierig die Spezialfälle zu erkennen, bei welchen mehr als 3 Argumente benötigt werden

## FAQ

### Ist es nach dieser Regel strikt verboten, eine Funktion mit mehr als 3 Argumenten zu verwenden?

Antwort: Nein das ist nicht strikt verboten. Es kann Spezialfälle geben, in denen ein Problem nicht anders gelöst werden kann. Diese sind jedoch sehr selten. Sollten sie trotzdem auftreten, empfiehlt es sich diese genau zu dokumentieren, damit ersichtlich ist, warum diese mehr als 3 Funktionsargumente benötigt.
