import { CleanCodeConfig } from "../../clean-code-config";
import { Node, VariableNode } from "../../parser/parser/nodes/_index";
import { Dictionary } from "../../resources/dictionary";
import { IdentifierNameRule } from "../rules/_index";
import { Validator } from "./validator";

export class VariableNameValidator implements Validator {
  private rule: IdentifierNameRule;

  constructor(dictionary: Dictionary) {
    this.rule = new IdentifierNameRule(dictionary);
  }

  validate(nodes: Array<Node>, settings: CleanCodeConfig): void {
    nodes.forEach((node) => {
      if (node instanceof VariableNode) {
        this.rule.checkRule(node, settings);
      }
    });
  }
}
