import { Dictionary } from "../../resources/dictionary";

export class IdentifierHelper {
  private dictionary: Dictionary;

  constructor(dictionary: Dictionary) {
    this.dictionary = dictionary;
  }

  identifierToWords(identifier: string): Array<string> {
    return (
      identifier
        // replaces numbers with blanks
        .replace(/(\d+)/g, " ")
        // adds blank between lowercase letter followerd by uppercase letter
        .replace(/([a-z]+)([A-Z])/g, "$1 $2")
        // adds blank before last uppercase letter, if there are multiple uppercase letters followed by one or more lowercase letters
        .replace(/([A-Z]+)([A-Z])([a-z]+)/g, "$1 $2$3")
        // replaces underscore with blank
        .replace(/[_]/g, " ")
        // replaces multiple blanks with one blank
        .replace(/\s{2,}/, " ")
        .trim()
        .split(" ")
    );
  }

  isWord(word: string): boolean {
    return (
      this.dictionary.dictionary?.some(
        (value) => value.word.toLowerCase() === word.toLowerCase()
      ) ?? false
    );
  }

  isWordType(word: string, type: WordType): boolean {
    const entry = this.dictionary.dictionary?.filter(
      (value) => value.word.toLowerCase() === word.toLowerCase()
    )[0];
    if (entry) {
      return entry.definitions.some(
        (value) => value.partOfSpeech === WordType[type]
      );
    } else {
      return false;
    }
  }
}

export enum WordType {
  "noun" = 1,
  "verb",
}
