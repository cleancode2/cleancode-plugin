import { Range } from "vscode-languageserver";
import { Token } from "./token";

export class NumberToken extends Token {
  private _value: number;

  constructor(value: number, range: Range) {
    super(range);
    this._value = value;
  }

  get value(): number {
    return this._value;
  }
}
