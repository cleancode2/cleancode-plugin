import { Range } from "vscode-languageserver";
import { Token } from "./token";

export class StringToken extends Token {
  private _value: string;

  constructor(value: string, range: Range) {
    super(range);
    this._value = value;
  }

  get value(): string {
    return this._value;
  }
}
