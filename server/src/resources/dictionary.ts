export interface Dictionary {
  dictionary?: Array<DictionaryEntry>;
}

interface DictionaryEntry {
  word: string;
  definitions: Array<WordDefinition>;
}

interface WordDefinition {
  definition: string;
  partOfSpeech: string;
}
