# Naming of variables and functions

## Summary

Using clear and readable identifiers for variables and functions help to make code more understandable for everyone. The more descriptive an identifier is, the better. For example, class names should start with a noun and functions with a verb. Any kind of encoding of identifiers should be avoided. Applying these principles also improves searchability, which also makes it easier for other programmers to quickly understand the code.

### Problem description

#### Clear names

Names should be pronounceable so that our brain can deal with them more easily. This means in particular the avoidance of little-known acronyms and abbreviations, but also the omission of individual letters from a word. Because these circumstances lead to the fact that our brain has more difficulties to process the source text "fluently".

Encoding should also be avoided for the same reasons, as the additional decoding makes reading and interpretation even more difficult. The same applies to own (company-internal) encoding.

#### Searchability

When programming (especially with larger code bases), it may be necessary to search for a certain variable or function whose name you do not know exactly. Here it helps if descriptive, complete names (i.e. without abbreviations, acronyms, etc.) are used.

## Solution

Pronounceable and searchable names must always be composed of complete words. All encodings are to be avoided. In addition, if possible, descriptive names should be used, as this further increases searchability.

## Code examples

### Positive examples

The following examples are variables and functions with meaningful, clear names that meet the requirements of this rule.

``` Java
    class Triangle(...){...}
    
    print(...){...}

    class Customer(...){...}

    getCustomer(...){...}
```

All names are composed of complete words, contain no coding and are therefore easily searchable.

### Negative examples

The following examples violate the naming rule.

Clear names:

``` Java
    class DtaRcrd {...}
```

This class name is encoded and is therefore composed of incomplete words. Thus it is also hardly searchable. A little better would be:

``` Java
    class DataRecord{...}
```

Now the name is composed of complete words, without encoding and is therefore more searchable. Unfortunately, "DataRecord" is not very specific. Every class is a DataRecord in the end. Assuming that the DataRecord is the data of a customer, a good solution would therefore be:

``` Java
    class Costumer{...}
```

Encoding:

``` Java
    public class Part {
        private String m_dsc: \\ textual description
        void setName(String name) {
            m_dsc = name;
        }
    }
```

This code snippet is not intuitive to understand. Clearly better would be:

``` Java
    public class Part {
        String description;
        void setDescription(String description) {
            this.description = description;
        }
    }
```

## Advantages of the rule

These benefits arise when following this rule:

- Code can be read & understood more easily
- If the code is to be changed, the corresponding variables / functions can be found more quickly.
- The purpose of variables and functions is intuitively obvious to everyone who is reading the code.

## Disadvantages of the rule

These disadvantages arise when following this rule:

- Higher initial effort during development
- A little more "typing" necessary (but most IDEs today have auto-complete functions)

## FAQ

### Why should I not use "common" abbreviations such as (Desc for Description)?

Answer: Because even generally known abbreviations are not necessarily known to the same extent by every developer. For the human brain it is always more difficult to work with encoded names than with fully written out names.

### Are there cases where this rule should not / cannot be observed?

Answer: Yes, on systems where there is extremely little memory available and the source code must be shortened accordingly. There are also systems where all possible variable names are predefined.
