import { Range } from "vscode-languageserver";
import { Token } from "./token";

export class IdentifierToken extends Token {
  private _name: string;

  constructor(name: string, range: Range) {
    super(range);
    this._name = name;
  }

  get name(): string {
    return this._name;
  }
}
