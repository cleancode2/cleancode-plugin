# Contributing to the Clean Code Extension

First off all, thanks for taking the time to contribute!

The following is a set of guidelines for contributing to the Clean Code Extension. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

## Code of Conduct

This project and everyone participating in it is governed by the [Contributor Covenant](https://www.contributor-covenant.org/). For further Information please consult the [Code of conduct file](CODE_OF_CONDUCT.md).
Please report unacceptable behavior to the project maintainers.

## What should I know before I get started?

The Clean Code Extension is currently only developed for the [Visual Studio Code IDE](https://code.visualstudio.com/).

Support for other IDEs is currently not on our Roadmap. If you are interested in extending the Support for other Plugins feel free to reach out to the project maintainers.

The Plugin was intentionally build as modular as possible and attempts to comply with all object-oriented programming standards.

### Architecture

The extension is split into two parts, a client and a server. While the client is relatively light weight, the server itself is split into several subsystems, which perform their parts of the code validation independantly. These subsystems are tied together by a manager class that invokes whichever system is required for the next step. This design enables us to replace single subsystems if required. The client and server communicate through the Language Server Protocol over the Node inter-process communication module.

![System scope](docs/images/context-diagram.png)

### Design Decisions

Due to the potentially large amount of computation required to check all the rules for a larger file, we decided to implement the extension in the form of a language server extension.

We decided to use Typescript for the language server to develop all parts of the extension in the same language.

To ensure that no proprietary code snippets leave the premises, we decided to bundle a dictionary with the extension instead of checking against an external API.

## How Can I Contribute?

### Reporting Bugs

This section guides you through submitting a bug report for the clean code extension. Following these guidelines helps maintainers and the community understand your report, reproduce the behavior, and find related reports.

> **_NOTE:_** If you find a Closed issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

#### Before Submitting A Bug Report

Perform a cursory search in the issues section, to see if the problem has already been reported. If it has and the issue is still open, add a comment to the existing issue instead of opening a new one.

#### How Do I Submit A (Good) Bug Report?

Bugs are tracked as Gitlab Issues. Create an Issue in the Issue section of this repository and provide the following information by filling in the template.

Explain the problem and include additional details to help maintainers reproduce the problem:

- **Use a clear and descriptive title for the issue to identify the problem.**
- **Describe the exact steps which reproduce the problem in as many details as possible.**
- **Provide specific examples to demonstrate the steps.** Include links to files or Git projects, or copy/pasteable snippets, which you use in those examples. If you're providing snippets in the issue, use [Markdown code blocks](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github#multiple-lines).
- **Describe the behavior you observed after following the steps** and point out what exactly is the problem with that behavior.
- **Explain which behavior you expected to see instead and why.**
- **Include screenshots and animated GIFs** which show you following the described steps and clearly demonstrate the problem.
- **If you're reporting that the plugin crashed**, include a crash report with a stack trace that is provided by the VS Code IDE.
- **If the problem is related to performance or memory**, include a [CPU profile capture] with your report.
- **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened and share more information using the guidelines below.

Provide more context by answering these questions:

- **Did the problem start happening recently** (e.g. after updating to a new version of the extension) or was this always a problem?
- **If the problem started happening recently, can you reproduce the problem in an older version of the extension?** What's the most recent version in which the problem doesn't happen?
- **Can you reliably reproduce the issue?** If not, provide details about how often the problem happens and under which conditions it normally happens.
- **If the problem is related to working with specific files**, does the problem happen for all files and projects or only some?

Include details about your configuration and environment:

- **Which version of the extension are you using?**
- **What's the name and version of the OS you're using?**

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for the clean code extension, including completely new features and minor improvements to existing functionality. Following these guidelines helps maintainers and the community understand your suggestion and find related suggestions.

When you are creating an enhancement suggestion, please include as many details as possible.

### Before Submitting An Enhancement Suggestion

- Perform a cursory search to see if the enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.

### How Do I Submit A (Good) Enhancement Suggestion?

Enhancement suggestions are tracked as Gitlab issues. After you've determined which repository your enhancement suggestion is related to, create an issue on that repository and provide the following information:

- **Use a clear and descriptive title for the issue to identify the suggestion.**
- **Provide a step-by-step description of the suggested enhancement** in as many details as possible.
- **Provide specific examples to demonstrate the steps.**
- **Describe the current behavior and explain which behavior you expected to see instead** and why.
- **Explain why this enhancement would be useful** to most of the Users of the clen code extension.
- **List some other extensions or applications** where this enhancement allready exists. If there exist any.

### Your First Code Contribution

See [this link](https://github.com/firstcontributions/first-contributions) to learn about making your first code contribution. There is also [this blogpost](https://about.gitlab.com/blog/2016/06/16/fearless-contribution-a-guide-for-first-timers/) that goes more into the motivation behind and different ways of contributing to an open source project.

### Merge Requests

The process described here has several goals:

- Maintain the extensions quality
- Fix problems that are important to users
- Engage the community in working toward the best possible clean code related extension
- Enable a sustainable system for the extensions maintainers to review contributions

Please follow these steps to have your contribution considered by the maintainers:

- Use [Git Flow](https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow) and [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
- Include the Issue Number you are working on in this commit at the beginning of the branch name.
- If several issues are worked on in the same branch, refer only the most important one.
- Create a Merge Request into de develop branch and check for any merge conflicts.
- Solve all potentially emerging merge conflicts.

## Attribution

This Contributing File is adapted from the [Atom Contributing File](https://github.com/atom/atom/blob/master/CONTRIBUTING.md).
