import Ajv2020 from "ajv/dist/2020";
import data from "../../resources/dictionary.json";
import schema from "../../resources/dictionary.schema.json";
import chai from "chai";
chai.should();

suite("Dictionary Test Suite", () => {
  test("Dictionary is valid", () => {
    const ajv: Ajv2020 = new Ajv2020();
    const valid: boolean = ajv.validate(schema, data);

    valid.should.be.true;
  });
});
