import { RuleViolation, RuleViolations } from "./rule-violations/_index";
import { Node } from "../parser/parser/nodes/_index";
import {
  ClassNameValidator,
  Validator,
  VariableNameValidator,
  MethodNameValidator,
  FunctionArgumentCountValidator,
  LawOfDemeterValidator,
} from "./validators/_index";
import { CleanCodeConfig } from "../clean-code-config";
import { Dictionary } from "../resources/dictionary";

export class ValidationManager {
  static ruleViolations: RuleViolations = new RuleViolations();
  private validators: Array<Validator> = new Array<Validator>();

  constructor(dictionary: Dictionary) {
    this.addValidators(dictionary);
  }

  validateRules(
    nodes: Array<Node>,
    settings: CleanCodeConfig
  ): Array<RuleViolation> {
    ValidationManager.ruleViolations.clear();

    this.validators.forEach((validator) => {
      validator.validate(nodes, settings);
    });

    return ValidationManager.ruleViolations.ruleViolations;
  }

  private addValidators(dictionary: Dictionary): void {
    this.validators.push(new ClassNameValidator(dictionary));
    this.validators.push(new VariableNameValidator(dictionary));
    this.validators.push(new MethodNameValidator(dictionary));
    this.validators.push(new FunctionArgumentCountValidator());
    this.validators.push(new LawOfDemeterValidator());
  }
}
