import { Range } from "vscode-languageserver";

export class RangeHelper {
  private lineStart = 0;
  private lineEnd = 0;
  private charStart = -1;
  private charEnd = -1;

  setStart(
    lineStart: number,
    lineEnd: number,
    charStart: number,
    charEnd: number
  ): void {
    this.lineStart = lineStart;
    this.lineEnd = lineEnd;
    this.charStart = charStart;
    this.charEnd = charEnd;
  }

  setEnd(lineEnd: number, charEnd: number): void {
    this.lineEnd = lineEnd;
    this.charEnd = charEnd;
  }

  get range(): Range {
    return {
      start: { line: this.lineStart, character: this.charStart },
      end: { line: this.lineEnd, character: this.charEnd },
    };
  }
}
