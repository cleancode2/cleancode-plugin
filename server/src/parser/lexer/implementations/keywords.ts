import { Tag } from "../helper/tag";

export interface Keywords {
  isKeyword(keyword: string): boolean;
  getTag(keyword: string): Tag;
}
