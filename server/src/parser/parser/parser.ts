import { TextDocument } from "vscode-languageserver-textdocument";
import { Node } from "./nodes/_index";

export interface Parser {
  parse(document: TextDocument): Array<Node>;
}
