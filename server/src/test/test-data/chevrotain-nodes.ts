import {
  IToken,
  MethodDeclaratorCtx,
  NormalClassDeclarationCtx,
  PrimaryCtx,
  VariableDeclaratorIdCtx,
} from "java-parser";
import { CstNodeLocation } from "chevrotain";

export const normalClassDeclarationCtx: NormalClassDeclarationCtx = {
  typeIdentifier: [
    {
      children: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Identifier: [
          {
            endColumn: 14,
            endLine: 1,
            endOffset: 13,
            image: "A",
            startColumn: 14,
            startLine: 1,
            startOffset: 13,
            tokenTypeIdx: 0,
            tokenType: {
              name: "tokenType",
            },
          },
        ],
      },
      name: "typeIdentifier",
      location: getLocation(0, 0, 0, 0),
    },
  ],
  // eslint-disable-next-line @typescript-eslint/naming-convention
  Class: [],
  classBody: [],
};

export const variableDeclaratorIdCtx: VariableDeclaratorIdCtx = {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  Identifier: [
    {
      endColumn: 22,
      endLine: 1,
      endOffset: 21,
      image: "x",
      startColumn: 22,
      startLine: 1,
      startOffset: 21,
      tokenTypeIdx: 0,
      tokenType: {
        name: "tokenType",
      },
    },
  ],
};

export const methodDeclaratorCtx: MethodDeclaratorCtx = {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  Identifier: [
    {
      endColumn: 38,
      endLine: 1,
      endOffset: 37,
      image: "test",
      startColumn: 35,
      startLine: 1,
      startOffset: 34,
      tokenTypeIdx: 0,
      tokenType: {
        name: "tokenType",
      },
    },
  ],
  // eslint-disable-next-line @typescript-eslint/naming-convention
  LBrace: [],
  // eslint-disable-next-line @typescript-eslint/naming-convention
  RBrace: [],
};

export const primaryCtx: PrimaryCtx = {
  primaryPrefix: [
    {
      children: {
        fqnOrRefType: [
          {
            children: {
              fqnOrRefTypePartFirst: [
                {
                  children: {
                    fqnOrRefTypePartCommon: [
                      {
                        children: {
                          // eslint-disable-next-line @typescript-eslint/naming-convention
                          Identifier: [getIdentifier("foo")],
                        },
                        name: "fqnOrRefTypePartCommon",
                        location: getLocation(0, 0, 0, 0),
                      },
                    ],
                  },
                  name: "fqnOrRefTypePartFirst",
                  location: getLocation(0, 0, 0, 0),
                },
              ],
              fqnOrRefTypePartRest: [
                {
                  children: {
                    fqnOrRefTypePartCommon: [
                      {
                        children: {
                          // eslint-disable-next-line @typescript-eslint/naming-convention
                          Identifier: [getIdentifier("bar")],
                        },
                        name: "fqnOrRefTypePartCommon",
                        location: getLocation(0, 0, 0, 0),
                      },
                    ],
                  },
                  name: "fqnOrRefTypePartRest",
                  location: getLocation(0, 0, 0, 0),
                },
              ],
            },
            name: "fqnOrRefType",
            location: getLocation(0, 0, 0, 0),
          },
        ],
      },
      name: "primaryPrefix",
      location: getLocation(0, 0, 0, 0),
    },
  ],
  primarySuffix: [
    {
      children: {
        methodInvocationSuffix: [],
      },
      name: "primarySuffix",
      location: getLocation(0, 0, 0, 0),
    },
    {
      children: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Identifier: [getIdentifier("test")],
      },
      name: "primarySuffix",
      location: getLocation(0, 0, 0, 0),
    },
    {
      children: {
        methodInvocationSuffix: [],
      },
      name: "primarySuffix",
      location: getLocation(0, 0, 0, 0),
    },
    {
      children: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Identifier: [getIdentifier("foobar")],
      },
      name: "primarySuffix",
      location: getLocation(0, 20, 0, 25),
    },
  ],
};

function getIdentifier(image: string): IToken {
  return {
    image: image,
    startOffset: 0,
    startLine: 0,
    startColumn: 0,
    endOffset: 0,
    endLine: 0,
    endColumn: 0,
    tokenTypeIdx: 0,
    tokenType: {
      name: "tokenType",
    },
  };
}

function getLocation(
  startLine: number,
  startColumn: number,
  endLine: number,
  endColumn: number
): CstNodeLocation {
  return {
    startOffset: 0,
    startLine: startLine,
    startColumn: startColumn,
    endLine: endLine,
    endColumn: endColumn,
  };
}
