import {
  Diagnostic,
  DiagnosticSeverity,
  Position,
  Range,
} from "vscode-languageserver";
import { defaultConfig, RuleConfigLevel } from "../../clean-code-config";
import { NotificationManager } from "../../notification/notification-manager";
import {
  ExpressionNode,
  MethodNode,
} from "../../parser/parser/nodes/_index";
import {
  BlackListViolation,
  FunctionArgumentCountRuleViolation,
  LawOfDemeterViolation,
  NamingRuleViolation,
  RuleViolation,
  WordTypeRuleViolation,
} from "../../validation/rule-violations/_index";
import { WordType } from "../../validation/helper/identifier-helper";
import chai from "chai";
chai.should();

const notificationManager = new NotificationManager();

suite("NotificationManager Test Suite", () => {
  test("getDiagnostics returns empty diagnostics array if there are no rule violations", () => {
    const ruleViolations: Array<RuleViolation> = [];
    const settings = defaultConfig;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    diagnostics.should.be.empty;
  });

  test("getDiagnostics returns empty diagnostics array if setting for blacklist violation is turned off", () => {
    const ruleViolations: Array<RuleViolation> = [blackListViolation];
    const settings = defaultConfig;
    settings.blacklistViolation = RuleConfigLevel.off;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    diagnostics.should.be.empty;
  });

  test("getDiagnostics returns empty diagnostics array if setting for function argument count rule violation is turned off", () => {
    const ruleViolations: Array<RuleViolation> = [
      functionArgumentCountRuleViolation,
    ];
    const settings = defaultConfig;
    settings.functionArgumentCountViolation = RuleConfigLevel.off;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    diagnostics.should.be.empty;
  });

  test("getDiagnostics returns empty diagnostics array if setting for law of demeter violation is turned off", () => {
    const ruleViolations: Array<RuleViolation> = [lawOfDemeterViolation];
    const settings = defaultConfig;
    settings.lawOfDemeterViolation = RuleConfigLevel.off;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    diagnostics.should.be.empty;
  });

  test("getDiagnostics returns empty diagnostics array if setting for naming rule violation is turned off", () => {
    const ruleViolations: Array<RuleViolation> = [namingRuleViolation];
    const settings = defaultConfig;
    settings.namingViolation = RuleConfigLevel.off;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    diagnostics.should.be.empty;
  });

  test("getDiagnostics returns empty diagnostics array if setting for word type rule violation is turned off", () => {
    const ruleViolations: Array<RuleViolation> = [wordTypeRuleViolation];
    const settings = defaultConfig;
    settings.wordTypeViolation = RuleConfigLevel.off;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    diagnostics.should.be.empty;
  });

  test("getDiagnostics returns correct diagnostic if setting for blacklist violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [blackListViolation];
    const settings = defaultConfig;
    settings.blacklistViolation = RuleConfigLevel.warning;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Warning);
  });

  test("getDiagnostics returns correct diagnostic if setting for function argument count rule violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [
      functionArgumentCountRuleViolation,
    ];
    const settings = defaultConfig;
    settings.functionArgumentCountViolation = RuleConfigLevel.warning;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Warning);
  });

  test("getDiagnostics returns correct diagnostic if setting for law of demeter violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [lawOfDemeterViolation];
    const settings = defaultConfig;
    settings.lawOfDemeterViolation = RuleConfigLevel.warning;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Warning);
  });

  test("getDiagnostics returns correct diagnostic if setting for naming rule violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [namingRuleViolation];
    const settings = defaultConfig;
    settings.namingViolation = RuleConfigLevel.warning;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Warning);
  });

  test("getDiagnostics returns correct diagnostic if setting for word type rule violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [wordTypeRuleViolation];
    const settings = defaultConfig;
    settings.wordTypeViolation = RuleConfigLevel.warning;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Warning);
  });

  test("getDiagnostics returns correct diagnostic if setting for blacklist violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [blackListViolation];
    const settings = defaultConfig;
    settings.blacklistViolation = RuleConfigLevel.error;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Error);
  });

  test("getDiagnostics returns correct diagnostic if setting for function argument count rule violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [
      functionArgumentCountRuleViolation,
    ];
    const settings = defaultConfig;
    settings.functionArgumentCountViolation = RuleConfigLevel.error;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Error);
  });

  test("getDiagnostics returns correct diagnostic if setting for law of demeter violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [lawOfDemeterViolation];
    const settings = defaultConfig;
    settings.lawOfDemeterViolation = RuleConfigLevel.error;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Error);
  });

  test("getDiagnostics returns correct diagnostic if setting for naming rule violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [namingRuleViolation];
    const settings = defaultConfig;
    settings.namingViolation = RuleConfigLevel.error;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Error);
  });

  test("getDiagnostics returns correct diagnostic if setting for word type rule violation is set to warning", () => {
    const ruleViolations: Array<RuleViolation> = [wordTypeRuleViolation];
    const settings = defaultConfig;
    settings.wordTypeViolation = RuleConfigLevel.error;
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      settings
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Error);
  });

  test("getDiagnostics returns warning if rule violation type is not recognized", () => {
    class TestViolation extends RuleViolation {}
    const ruleViolations: Array<RuleViolation> = [
      new TestViolation(methodNode, ""),
    ];
    const diagnostics = notificationManager.getDiagnostics(
      ruleViolations,
      defaultConfig
    );
    checkDiagnostic(diagnostics, DiagnosticSeverity.Warning);
  });
});

function checkDiagnostic(
  diagnostics: Array<Diagnostic>,
  severity: DiagnosticSeverity
) {
  diagnostics.should.be.length(1);
  diagnostics[0].severity?.should.be.equals(severity);
  diagnostics[0].range.start.should.be.equals(range.start);
  diagnostics[0].range.end.should.be.equals(range.end);
}

const range: Range = Range.create(Position.create(0, 0), Position.create(0, 1));

const methodNode: MethodNode = new MethodNode("testFunction", 0, range);
const expressionNode: ExpressionNode = new ExpressionNode([], range);

const blackListViolation: BlackListViolation = new BlackListViolation(
  methodNode,
  methodNode.name
);
const functionArgumentCountRuleViolation: FunctionArgumentCountRuleViolation =
  new FunctionArgumentCountRuleViolation(methodNode);
const lawOfDemeterViolation: LawOfDemeterViolation = new LawOfDemeterViolation(
  expressionNode
);
const namingRuleViolation: NamingRuleViolation = new NamingRuleViolation(
  methodNode,
  methodNode.name
);
const wordTypeRuleViolation: WordTypeRuleViolation = new WordTypeRuleViolation(
  methodNode,
  methodNode.name,
  WordType.noun
);
