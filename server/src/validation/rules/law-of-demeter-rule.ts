import { CleanCodeConfig, RuleConfigLevel } from "../../clean-code-config";
import {
  ExpressionNode,
  ExpressionPartType,
} from "../../parser/parser/nodes/_index";
import { LawOfDemeterViolation } from "../rule-violations/_index";
import { ValidationManager } from "../validation-manager";
import { Rule } from "./_index";

export class LawOfDemeterRule implements Rule {
  checkRule(node: ExpressionNode, settings: CleanCodeConfig): void {
    if (settings.lawOfDemeterViolation !== RuleConfigLevel.off) {
      node.parts.some((part, index) => {
        if (part.type === ExpressionPartType.methodCall && index > 1) {
          ValidationManager.ruleViolations.addViolation(
            new LawOfDemeterViolation(node)
          );
        }
      });
    }
  }
}
