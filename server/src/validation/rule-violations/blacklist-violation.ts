import { Node } from "../../parser/parser/nodes/_index";
import { RuleViolation } from "./_index";

export class BlackListViolation extends RuleViolation {
  constructor(node: Node, identifierPart: string) {
    super(
      node,
      `${identifierPart} is on the blacklist \nidentifiers should not be comprised of words that are banned`
    );
  }
}
