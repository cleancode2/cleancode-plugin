import { Range } from "vscode-languageserver";
import { Token } from "./token";

export class ErrorToken extends Token {
  constructor(range: Range) {
    super(range);
  }
}
