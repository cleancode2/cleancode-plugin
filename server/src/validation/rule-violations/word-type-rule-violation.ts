import { IdentifierNode } from "../../parser/parser/nodes/_index";
import { WordType } from "../helper/identifier-helper";
import { RuleViolation } from "./_index";
import dotenv from 'dotenv';
import path from "path";

export class WordTypeRuleViolation extends RuleViolation {
  constructor(node: IdentifierNode, identifierPart: string, wordType: WordType, identifierType?: string) {
    dotenv.config({ path: path.resolve(__dirname, '../../../../urls.env') });
    super(node, `${identifierPart} is not a ${WordType[wordType]} \n${identifierType} names should start with a ${WordType[wordType]}. Learn more at ${process.env.WIKIPAGE_BASE_URL}${process.env.NAMING_RULE_URL}`);
  }
}
