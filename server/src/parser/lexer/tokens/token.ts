import { Range } from "vscode-languageserver";

export abstract class Token {
  private _range: Range;

  constructor(range: Range) {
    this._range = range;
  }

  get range(): Range {
    return this._range;
  }
}
