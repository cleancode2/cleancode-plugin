import { Diagnostic, DiagnosticSeverity } from "vscode-languageserver/node";
import { CleanCodeConfig, RuleConfigLevel } from "../clean-code-config";
import {
  BlackListViolation,
  FunctionArgumentCountRuleViolation,
  LawOfDemeterViolation,
  NamingRuleViolation,
  RuleViolation,
  WordTypeRuleViolation,
} from "../validation/rule-violations/_index";

export class NotificationManager {
  getDiagnostics(
    ruleViolations: Array<RuleViolation>,
    settings: CleanCodeConfig
  ): Diagnostic[] {
    const diagnostics: Diagnostic[] = [];

    if (ruleViolations.length > 0) {
      ruleViolations.forEach((violation) => {
        const level = this.getRuleConfigLevel(violation, settings);
        if (level !== RuleConfigLevel.off) {
          diagnostics.push(this.getDiagnostic(violation, level));
        }
      });
    }
    return diagnostics;
  }

  private getRuleConfigLevel(
    violation: RuleViolation,
    settings: CleanCodeConfig
  ): RuleConfigLevel {
    if (violation instanceof BlackListViolation) {
      return settings.blacklistViolation;
    } else if (violation instanceof FunctionArgumentCountRuleViolation) {
      return settings.functionArgumentCountViolation;
    } else if (violation instanceof LawOfDemeterViolation) {
      return settings.lawOfDemeterViolation;
    } else if (violation instanceof NamingRuleViolation) {
      return settings.namingViolation;
    } else if (violation instanceof WordTypeRuleViolation) {
      return settings.wordTypeViolation;
    } else {
      return RuleConfigLevel.warning;
    }
  }

  private getDiagnostic(
    violation: RuleViolation,
    level: RuleConfigLevel
  ): Diagnostic {
    const severity =
      level === RuleConfigLevel.warning
        ? DiagnosticSeverity.Warning
        : DiagnosticSeverity.Error;
    const diagnostic: Diagnostic = {
      severity: severity,
      range: {
        start: violation.node.range.start,
        end: violation.node.range.end,
      },
      message: violation.errorMessage,
    };
    return diagnostic;
  }
}
