import { Range } from "vscode-languageserver";
import { Node } from "./_index";

export class ExpressionPartNode extends Node {
  private _type: ExpressionPartType;
  private _order: number;
  private _name: string;

  constructor(
    type: ExpressionPartType,
    order: number,
    name: string,
    range: Range
  ) {
    super(range);
    this._type = type;
    this._order = order;
    this._name = name;
  }

  get type(): ExpressionPartType {
    return this._type;
  }

  get order(): number {
    return this._order;
  }

  get name(): string {
    return this._name;
  }
}

export enum ExpressionPartType {
  methodCall,
  fieldCall,
}
