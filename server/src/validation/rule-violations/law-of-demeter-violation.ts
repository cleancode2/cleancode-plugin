import { ExpressionNode } from "../../parser/parser/nodes/_index";
import { RuleViolation } from "./_index";
import dotenv from 'dotenv';
import path from "path";

export class LawOfDemeterViolation extends RuleViolation {
  constructor(node: ExpressionNode) {
    dotenv.config({ path: path.resolve(__dirname, '../../../../urls.env') });
    super(node, `Possible violation of the Law of Demeter (too strong coupling between multiple Objects). Learn more at ${process.env.WIKIPAGE_BASE_URL}${process.env.LAW_OF_DEMETER_RULE_URL}`);
  }
}
