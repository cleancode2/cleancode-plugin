import { TextDocument } from "vscode-languageserver-textdocument";
import { Node } from "../../nodes/_index";
import { Parser } from "../../parser";
import { CleanCodeVisitor } from "./clean-code-visitor";
import { parse } from "java-parser";

export class JavaParser implements Parser {
  parse(document: TextDocument): Array<Node> {
    try {
      const concreteSyntaxTree = parse(document.getText());
      const cleanCodeVisitor = new CleanCodeVisitor();
      cleanCodeVisitor.visit(concreteSyntaxTree);
      return cleanCodeVisitor.nodes;
    } catch {
      return [];
    }
  }
}
