import { CleanCodeConfig, RuleConfigLevel } from "../../clean-code-config";
import { IdentifierNode } from "../../parser/parser/nodes/_index";
import { Dictionary } from "../../resources/dictionary";
import { IdentifierHelper, WordType } from "../helper/identifier-helper";
import {
  BlackListViolation,
  NamingRuleViolation,
  WordTypeRuleViolation,
} from "../rule-violations/_index";
import { ValidationManager } from "../validation-manager";
import { Rule } from "./rule";

export class IdentifierNameRule implements Rule {
  private identifierHelper: IdentifierHelper;

  constructor(dictionary: Dictionary) {
    this.identifierHelper = new IdentifierHelper(dictionary);
  }

  checkRule(
    node: IdentifierNode,
    settings: CleanCodeConfig,
    firstWord?: WordType,
    type?: string
  ): void {
    const words = this.identifierHelper.identifierToWords(node.name);

    if (firstWord) {
      this.checkWordType(node, settings, firstWord, words, type);
    }

    this.checkNaming(node, settings, words);
  }

  private checkWordType(node: IdentifierNode, settings: CleanCodeConfig, firstWord: WordType, words: Array<string>, type?: string) {
    const blackListViolation = this.checkBlackListViolation(node, settings, words[0]);

    if (settings.wordTypeViolation !== RuleConfigLevel.off && !blackListViolation) {
      if (!this.identifierHelper.isWordType(words[0], firstWord)) {
        if (!settings.whitelist.some((value) => value.toLocaleLowerCase() === words[0].toLocaleLowerCase())) {
          ValidationManager.ruleViolations.addViolation(
            new WordTypeRuleViolation(node, words[0], firstWord, type)
          );
        }
      }
    }
    words.shift();
  }

  private checkNaming(node: IdentifierNode, settings: CleanCodeConfig, words: Array<string>) {
    words.some((word) => {
      const blackListViolation = this.checkBlackListViolation(node, settings, word);
      if (settings.namingViolation !== RuleConfigLevel.off && !blackListViolation) {
        if (!this.identifierHelper.isWord(word)) {
          if (
            !settings.whitelist.some(
              (value) => value.toLocaleLowerCase() === word.toLocaleLowerCase()
            )
          ) {
            ValidationManager.ruleViolations.addViolation(
              new NamingRuleViolation(node, word)
            );
          }
        }
      }
    });
  }

  private checkBlackListViolation(node: IdentifierNode, settings: CleanCodeConfig, word: string): boolean {
    if (settings.blacklistViolation !== RuleConfigLevel.off) {
      if (
        settings.blacklist.some(
          (value) => value.toLocaleLowerCase() === word.toLocaleLowerCase()
        )
      ) {
        ValidationManager.ruleViolations.addViolation(
          new BlackListViolation(node, word)
        );
        return true;
      }
    }
    return false;
  }
}
