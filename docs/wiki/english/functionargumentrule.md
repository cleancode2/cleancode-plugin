# Number of parameters of a function

## Summary

According to Robert C. Martin, the optimal number of arguments for a function is 0. 1 and 2 arguments are also OK. A third argument should be avoided if possible. More than three arguments should never be used. This is justified by the great conceptual imagination required to understand each additional argument. Furthermore, as the number of arguments increases, it becomes more and more difficult to test the function in a meaningful way. The software should be adapted accordingly.

## Problem description

The conceptual understanding of arguments is difficult for our brain. Even one argument enormously increases the demands on the conceptual understanding (imagination) of the human brain compared to no argument. This difficulty increases exponentially as the number of arguments increases. A function with no arguments is also much easier to test than a function with one argument. The more arguments a function has, the greater the effort required to test a function meaningfully and completely. Both facts lead to greater time expenditure & lower development efficiency and thus to higher development costs.

## Solution

If possible, functions should be used without arguments.

In cases where this is not possible, functions with an argument should be used. In more special cases, it is also OK to use 2 or max. 3 arguments. If 3 or more arguments are needed to achieve the desired effect, this indicates a major problem and one should think about refactoring this part of the software.

If a function takes 2 or more arguments to work, it is likely that some of these arguments should be wrapped in a separate class:

### Example

In the following function:

``` Java
    void print_circle(double x, double y, double radius){...}
```

A new class Point can be created for the two double coordinates x & y.

``` Java
    class Point(double x, double y){...}
```

By which the number of arguments can be reduced from 3 to 2:

``` Java
    void print_circle(Point point, double radius){..}
```

## Code examples

### Positive examples

Function with 0 arguments:

``` Java
    void print_hello(){
        print("hello");
    }

Function with 1 argument:

    void print_shape(shape){
        print(shape);
    }
    print(circle)

Function with 2 arguments:

    void print_circle(Point point, double radius){...}
```

### Negative example

Function with 4 arguments:

``` Java
    void print_triangle(double x, double y, double z, string colour){...}
```

Here, all 4 arguments can be wrapped in their own class:

``` Java
    class Triangle(double x, double y, double z, string colour){...}
```

And this reduces the number of function arguments to 1:

``` Java
    void print_triangle(Triangle triangle){...}
```

## Advantages of the rule

These advantages arise when following this rule:

- Code can be read & understood more easily
- The object structuring improves automatically

## Disadvantages of the rule

These disadvantages arise when following this rule:

- Higher initial effort during development
- It is difficult to identify the special cases where more than 3 arguments are needed.

## FAQ

### According to this rule, is it strictly forbidden to use a function with more than 3 arguments?

Answer: No, it is not strictly forbidden. There may be special cases where a problem cannot be solved in any other way. However, these are very rare. If they do occur, it is advisable to document them precisely so that it is clear why they require more than 3 function arguments.
