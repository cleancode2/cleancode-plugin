# Law Of Demeter

## Summary

The Law Of Demeter is a rule of object-oriented software development. It states that objects should only communicate with other objects from their immediate environment. As an analogy, the principle "Speak only to your closest friends, not to strangers" is often cited. This rule is intended to reduce coupling within the software and thus increase maintainability.

More precisely, the Law Of Demeter states that a method f of a class K should only call methods of the following components:

- K
- Objects created by f
- Objects that are passed as arguments to f
- Objects that are contained in an instance variable of K.

Furthermore, no methods should be called on objects returned by these allowed methods.
  
If violations of the Law Of Demeter are detected, they can usually be remedied by encapsulating objects in a refactoring.

### Problem description

If the Law of Demeter is not adhered, confusion can quickly arise. This confusion could lead to hybrid structures, which are half object and half data structure. These hybrid structures are then characterised by the fact that they offer functions that fulfil important tasks and at the same time have public variables or mutators that reveal the private variables. This can then tempt external functions to use these variables as a procedural programme would, which does not correspond to the basic idea of object orientation.

Violating the Law of Demeter also automatically means increased coupling within software components, which makes the software less maintainable.

Example:

``` Java
    var address = account.getAccountHolder().getAddress();
```

In the given example, the Law Of Demeter is violated because first the method getAccountholder() is called from the object Account, which returns an Accountholder object. On this object, a separate method (getAddress()) is then called again. Thus, a "stranger" is spoken to, since only Account can be regarded as an object from the immediate environment.

## Solution

Encapsulation can be used as a solution against violations of the Law Of Demeter. For this, wrapper methods are implemented which delegate the call to the foreign class.

Example:

``` Java
    class Motor {
        public void start() {
            // start the motor
        }
    }

    class Car {
        private Motor motor;
        public Car() {
            motor = new Motor();
        }
        public Motor getMotor() {
            return motor;
        }
    }
    class Driver {
        public void drive() {
            Car car = new Car();
            car.getMotor().start(); // Violation of Law of Demeter
        }
    }
```

To remove the violation of the Law Of Demeter, a wrapper method would be added to the Car class here, delegating the call to the Motor class.

``` Java
 class Motor {
        public void start() {
           // start the motor
       }
    }
    class Car {
        private Motor motor;
        public Car() {
            motor = new Motor();
        }
        public void readyCar() {
            motor.start();
        }
    }
    class Driver {
        public void drive() {
            Car car = new Car();
            car.readyCar();
        }
    }
```

The advantage of this is that the readyCar() method can be modified as required without the caller needing to know any further details about the method.

## Code examples

### Positive examples

``` Java
    public class LawOfDemeter{
        private Topping cheeseTopping;
  

        public void goodExamples(Pizza pizza){
            Foo foo = new Foo();
    
            // (1) it's okay to call our own methods
            doSomething();
    
            // (2) it's okay to call methods on objects passed in to our method
            int price = pizza.getPrice();
    
            // (3) it's okay to call methods on any objects we create
            cheeseTopping = new CheeseTopping();
            float weight = cheeseTopping.getWeightUsed();
    
            // (4) any directly held component objects
            foo.doBar();
        }
  
        private void doSomething()
        {
            // do something here ...
        }
    }
```

### Negative examples

``` Java
    var data = new A().GetObjectB().GetObjectC().GetData();
    
    customer.getMailingAddress().getLine1();
  
    employee.getHomeAddress().getLine1();
```

## Advantages of the rule

These benefits arise when following this rule:


- Coupling is reduced
- Maintainability is increased
- Classes can be reused more easily
- Testability is improved
- Classes that follow the Law Of Demeter are less likely to exhibit erroneous behaviour

## Disadvantages of the rule

These disadvantages arise when following this rule:

- Greater initial programming effort
- Minimal negative impact on performance and slightly higher memory consumption at runtime

## FAQ

### When using the Java Stream API, I am shown a violation of the Law Of Demeter. Does this mean that I am not allowed to use the Stream API for clean code?

Answer: No, it does not mean that. The Java Stream API can be used to manipulate elements in a functional programming style. Since these are functional approaches, this does violate object-oriented principles such as the Law Of Demeter, but this does not automatically mean that the code is bad as a result. The Stream API is a tool provided by Java and can therefore be used without any problems.

We will provide a setting in the future to disable notifications of Law Of Demeter violations when using the Stream API.
