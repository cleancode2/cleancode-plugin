import { TextDocument } from "vscode-languageserver-textdocument";
import { Token } from "./tokens/token";

export interface Lexer {
  scanDocument(document: TextDocument): Array<Token>;
}
