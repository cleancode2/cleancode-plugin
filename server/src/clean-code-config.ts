export interface CleanCodeConfig {
  maxNumberOfProblems: number;
  blacklist: Array<string>;
  whitelist: Array<string>;
  blacklistViolation: RuleConfigLevel;
  functionArgumentCountViolation: RuleConfigLevel;
  lawOfDemeterViolation: RuleConfigLevel;
  namingViolation: RuleConfigLevel;
  wordTypeViolation: RuleConfigLevel;
}

export enum RuleConfigLevel {
  "off" = "off",
  "warning" = "warning",
  "error" = "error",
}

export const defaultConfig: CleanCodeConfig = {
  maxNumberOfProblems: 100,
  blacklist: [],
  whitelist: [],
  blacklistViolation: RuleConfigLevel.warning,
  functionArgumentCountViolation: RuleConfigLevel.warning,
  lawOfDemeterViolation: RuleConfigLevel.warning,
  namingViolation: RuleConfigLevel.warning,
  wordTypeViolation: RuleConfigLevel.warning,
};
