# Clean Code Extension
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=cleancode2_cleancode-plugin&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=cleancode2_cleancode-plugin) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=cleancode2_cleancode-plugin&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=cleancode2_cleancode-plugin) [![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=cleancode2_cleancode-plugin&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=cleancode2_cleancode-plugin) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=cleancode2_cleancode-plugin&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=cleancode2_cleancode-plugin) [![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=cleancode2_cleancode-plugin&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=cleancode2_cleancode-plugin) [![Coverage](https://sonarcloud.io/api/project_badges/measure?project=cleancode2_cleancode-plugin&metric=coverage)](https://sonarcloud.io/summary/new_code?id=cleancode2_cleancode-plugin)

Following clean code guidelines improves readability and maintainability of software projects. This project aims to make clean code rules available as a Visual Studio Code extension.

## Current Status

Please note that the plugin is still under development.

At present several rules laid out by Robert C. Martin in his book "Clean Code" are implemented and the extension can currently analyze Java files.

An overview of the set of verifiable Clean Code rules can be found in the [wiki](docs/wiki/).

An overview of further planned features can be found in the [roadmap](ROADMAP.md).


<!-- Anforderungen an die Entwicklungsumgebungen für die Integration ? -->

## Installation

The extension can be installed through the [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=fuhrer-schneider.cleancode). This is the preferred way how the extension should be installed for normale usage.

Alternatively the extension can also be build and installed locally.
To build the extension locally from this repository [nodejs](https://nodejs.org/en/download/) and [vsce](https://github.com/microsoft/vscode-vsce) need to be present on the system. Firstly the required node modules have to be installed. Open a bash or cmd window, navigate to the root folder of the repository and enter the following command:

```bash
npm install
```

After that the extension can be packaged with:

```bash
vsce package
```

A *cleancode-x.x.x.vsix* files should now be present in the folder. This file can be installed in Visual Studio Code manually by navigating to the Extensions menu (Ctrl+Shift+X). There you can click on the three dots opening the "More Actions" menu. The "Install from VSIX..." option opens a file browser where the generated package can be selected and installed.

<!-- TODO: Link Anpassen"  -->

![Install from VSIX](docs/images/vscode-install-extension-locally.jpg)

## Settings

The extension offers the following settings:

* maxNumberOfProblems: This setting caps the number of problems that the language server returns to the IDE. The default value is 100.
* blacklist: Add words to this list that you want to exclude from the dictionary.
* whitelist: Add words to this list that you want to extend the dictionary with.
* A setting for each possible rule violation where you can configure whether that violation should always be ignored, highlighted as a warning or highlighted as an error. The default setting is warning for all violations.
  * blacklistViolation
  * functionArgumentCountViolation
  * lawOfDemeterViolation
  * namingViolation
  * wordTypeViolation

The blacklist takes precedent over the whitelist in case a word is added to both.

To add words to the black- and whitelist the editor context menu features convenient commands.

![Editor Context Menu](docs/images/editor-context-menu.jpg)

If there is a need to share these settings between a team there is a possibility to use a configuration file inside the root folder of your project. Please note, that this is currently the only working and officially supported option for doing the configuration of the plugin. The file has to be named __cleancodeconfig.json__ and feature the following structure ([see example file](docs/cleancodeconfig.json)):

```json
{
    "maxNumberOfProblems": 100,
    "blacklist": [
        "foo",
        "bar"
    ],
    "whitelist": [
        "api"
    ],
    "blacklistViolation": "warning",
    "functionArgumentCountViolation": "warning",
    "lawOfDemeterViolation": "warning",
    "namingViolation": "warning",
    "wordTypeViolation": "warning"
}

// Possible configuration levels
{
    ...
    "blacklistViolation": "off",
    "blacklistViolation": "warning",
    "blacklistViolation": "error"
    ...
}
```

The extension will ignore user and workspace settings if a configuration file is present.

## Technologies

The extension as well as the language server are written in TypeScript.

The testing framework [Mocha](https://mochajs.org/) is used for unit testing in combination with the assertion library [Chai](https://www.chaijs.com/). To calculate test coverage [Istanbul](https://istanbul.js.org/) is utilized.

All the code and documentation is held in a public [Git Repository](https://gitlab.com/cleancode2/cleancode-plugin) which is hosted on Gitlab.

## Contributing

Please consult the [contributing file](CONTRIBUTING.md) in the repository if you wish to contribute to this project.

## Known Issues

* Law of Demeter check needs to be refined.
  * Currently, only a rudimentary implementation is available, which leads to many false positives.
  * As there is no possibility to suppress error messages yet, the problem can be solved by deactivating the rule via the configuration file.
  
* When using the VS Code internal configuration options, problems may occur.
  * This is because VS Code knows several levels (Global, Workspace, Project) on which the configuration is applied differently. This is why there can be confusion.
  * We recommend using the separate configuration file to circumvent these problems.

## FAQ

### A word I want to use in my identifiers is displayed as "is not a word" in the problem logs. What can I do?

This indicates that the word is missing in our dictionary. If the word is commonly known and used in the english language, please open an issue with the word, its word type and a brief definition of the word, so that we can add it to the dictionary.
Of course, you can also add the word to the dictionary yourself and open a merge request.

However, as a temporary solution, the word can be added to the whitelist, then the error message will disappear.

## Copyright and licensing

This repository is published under the [Apache License 2.0](LICENSE).
