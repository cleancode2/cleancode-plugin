import { RuleViolation } from "./rule-violation";

export class RuleViolations {
  private _ruleViolations: Array<RuleViolation>;

  constructor() {
    this._ruleViolations = new Array<RuleViolation>();
  }

  addViolation(ruleViolation: RuleViolation): void {
    this._ruleViolations.push(ruleViolation);
  }

  get ruleViolations(): Array<RuleViolation> {
    return this._ruleViolations;
  }

  clear(): void {
    this._ruleViolations = new Array<RuleViolation>();
  }
}
