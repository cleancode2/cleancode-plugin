import {
  IdentifierHelper,
  WordType,
} from "../../validation/helper/identifier-helper";
import {
  BlackListViolation,
  FunctionArgumentCountRuleViolation,
  LawOfDemeterViolation,
  NamingRuleViolation,
  WordTypeRuleViolation,
} from "../../validation/rule-violations/_index";
import { RuleViolations } from "../../validation/rule-violations/rule-violations";
import {
  Node,
  MethodNode,
  ClassNode,
  VariableNode,
  ExpressionNode,
  ExpressionPartNode,
  ExpressionPartType,
} from "../../parser/parser/nodes/_index";
import { ValidationManager } from "../../validation/validation-manager";
import {
  FunctionArgumentCountRule,
  IdentifierNameRule,
  LawOfDemeterRule,
} from "../../validation/rules/_index";
import {
  ClassNameValidator,
  FunctionArgumentCountValidator,
  LawOfDemeterValidator,
  MethodNameValidator,
  VariableNameValidator,
} from "../../validation/validators/_index";
import chai from "chai";
import {
  CleanCodeConfig,
  defaultConfig,
  RuleConfigLevel,
} from "../../clean-code-config";
import { Position, Range } from "vscode-languageserver";
import { dictionary } from "../test-data/test-dictionary";
import dotenv from "dotenv";
import path from "path";
chai.should();

suite("Validation Test Suite", () => {
  suite("IdentifierHelper Test Suite", () => {
    const identifierHelper = new IdentifierHelper(dictionary);
    test("identifierToWords numbers at beginning", () => {
      const identifier = "12abc";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(1);
      words[0].should.be.equal("abc");
    });

    test("identifierToWords numbers in between", () => {
      const identifier = "a1b2c";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(3);
      words[0].should.be.equal("a");
      words[1].should.be.equal("b");
      words[2].should.be.equal("c");
    });

    test("identifierToWords numbers at end", () => {
      const identifier = "abc12";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(1);
      words[0].should.be.equal("abc");
    });

    test("identifierToWords lowercase followed by one uppercase", () => {
      const identifier = "abcAbc";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(2);
      words[0].should.be.equal("abc");
      words[1].should.be.equal("Abc");
    });

    test("identifierToWords lowercase followed by several uppercase", () => {
      const identifier = "abcABC";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(2);
      words[0].should.be.equal("abc");
      words[1].should.be.equal("ABC");
    });

    test("identifierToWords multiple uppercase letters in beginning", () => {
      const identifier = "ABCAbc";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(2);
      words[0].should.be.equal("ABC");
      words[1].should.be.equal("Abc");
    });

    test("identifierToWords multiple uppercase letters in middle", () => {
      const identifier = "abcABCAbc";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(3);
      words[0].should.be.equal("abc");
      words[1].should.be.equal("ABC");
      words[2].should.be.equal("Abc");
    });

    test("identifierToWords multiple uppercase letters at end", () => {
      const identifier = "abcABCDEF";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(2);
      words[0].should.be.equal("abc");
      words[1].should.be.equal("ABCDEF");
    });

    test("identifierToWords underscore at beginning", () => {
      const identifier = "_abc";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(1);
      words[0].should.be.equal("abc");
    });

    test("identifierToWords underscore in middle", () => {
      const identifier = "abc_abc";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(2);
      words[0].should.be.equal("abc");
      words[1].should.be.equal("abc");
    });

    test("identifierToWords underscore at end", () => {
      const identifier = "abc_";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(1);
      words[0].should.be.equal("abc");
    });

    test("identifierToWords multiple underscores", () => {
      const identifier = "_abc__abc__";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(2);
      words[0].should.be.equal("abc");
      words[1].should.be.equal("abc");
    });

    test("identifierToWords whitespace at beginning", () => {
      const identifier = " abc";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(1);
      words[0].should.be.equal("abc");
    });

    test("identifierToWords whitespace at end", () => {
      const identifier = "abc ";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(1);
      words[0].should.be.equal("abc");
    });

    test("identifierToWords whitespace at beginning and end", () => {
      const identifier = " abc ";
      const words: Array<string> =
        identifierHelper.identifierToWords(identifier);
      words.length.should.be.equal(1);
      words[0].should.be.equal("abc");
    });

    test("isWord invalid word", () => {
      const word = "abcabcabc";
      const result: boolean = identifierHelper.isWord(word);
      result.should.be.false;
    });

    test("isWord valid word", () => {
      const word = "aardvark";
      const result: boolean = identifierHelper.isWord(word);
      result.should.be.true;
    });

    test("isWord valid word wrong case", () => {
      const word = "aArdVarK";
      const result: boolean = identifierHelper.isWord(word);
      result.should.be.true;
    });

    test("isWord misspellt word", () => {
      const word = "aaardvark";
      const result: boolean = identifierHelper.isWord(word);
      result.should.be.false;
    });

    test("isWordType invalid word", () => {
      const word = "abcabcabc";
      const type: WordType = WordType.noun;
      const result: boolean = identifierHelper.isWordType(word, type);
      result.should.be.false;
    });

    test("isWordType valid word correct type", () => {
      const word = "aardvark";
      const type: WordType = WordType.noun;
      const result: boolean = identifierHelper.isWordType(word, type);
      result.should.be.true;
    });

    test("isWordType valid word incorrect type", () => {
      const word = "aardvark";
      const type: WordType = WordType.verb;
      const result: boolean = identifierHelper.isWordType(word, type);
      result.should.be.false;
    });

    test("isWordType valid word wrong case correct type", () => {
      const word = "aArdVarK";
      const type: WordType = WordType.noun;
      const result: boolean = identifierHelper.isWordType(word, type);
      result.should.be.true;
    });

    test("isWordType misspellt word", () => {
      const word = "aaardvark";
      const type: WordType = WordType.noun;
      const result: boolean = identifierHelper.isWordType(word, type);
      result.should.be.false;
    });
  });

  suite("RuleViolations Test Suite", () => {
    test("constructor", () => {
      const ruleViolations: RuleViolations = new RuleViolations();
      ruleViolations.ruleViolations.should.not.be.null;
      ruleViolations.ruleViolations.should.not.be.undefined;
      ruleViolations.ruleViolations.should.be.empty;
    });

    test("addViolation", () => {
      const ruleViolations: RuleViolations = new RuleViolations();
      ruleViolations.addViolation(
        new NamingRuleViolation(new VariableNode("test", range), "test")
      );
      ruleViolations.ruleViolations.length.should.be.equal(1);
    });

    test("clear", () => {
      const ruleViolations: RuleViolations = new RuleViolations();
      ruleViolations.addViolation(
        new NamingRuleViolation(new VariableNode("test", range), "test")
      );
      ruleViolations.clear();
      ruleViolations.ruleViolations.should.be;
    });
  });

  suite("RuleViolation Test Suite", () => {
    test("FunctionArgumentCountRuleViolation", () => {
      const methodNode: MethodNode = new MethodNode("test", 4, range);
      const violation: FunctionArgumentCountRuleViolation =
        new FunctionArgumentCountRuleViolation(methodNode);

      dotenv.config({ path: path.resolve(__dirname, "../../../../urls.env") });
      violation.errorMessage.should.be.equal(
        `test has too many arguments (4)\nfunctions should not have more than 3 arguments. Learn more at ${process.env.WIKIPAGE_BASE_URL}${process.env.FUNCTION_ARGUMENT_RULE_URL}`
      );
    });

    test("NamingRuleViolation", () => {
      const variableNode: VariableNode = new VariableNode("test", range);
      const violation: NamingRuleViolation = new NamingRuleViolation(
        variableNode,
        "test"
      );

      dotenv.config({ path: path.resolve(__dirname, "../../../../urls.env") });
      violation.errorMessage.should.be.equal(
        `test is not a word \nidentifiers should be comprised of full words. Learn more at ${process.env.WIKIPAGE_BASE_URL}${process.env.NAMING_RULE_URL}`
      );
    });

    test("WordTypeRuleViolation", () => {
      const variableNode: ClassNode = new ClassNode("test", range);
      const violation: WordTypeRuleViolation = new WordTypeRuleViolation(
        variableNode,
        "test",
        WordType.noun,
        "class"
      );

      dotenv.config({ path: path.resolve(__dirname, "../../../../urls.env") });
      violation.errorMessage.should.be.equal(
        `test is not a noun \nclass names should start with a noun. Learn more at ${process.env.WIKIPAGE_BASE_URL}${process.env.NAMING_RULE_URL}`
      );
    });

    test("LawOfDemeterViolation", () => {
      const expressionNode: ExpressionNode = new ExpressionNode([], range);
      const violation: LawOfDemeterViolation = new LawOfDemeterViolation(
        expressionNode
      );

      dotenv.config({ path: path.resolve(__dirname, "../../../../urls.env") });
      violation.errorMessage.should.be.equal(
        `Possible violation of the Law of Demeter (too strong coupling between multiple Objects). Learn more at ${process.env.WIKIPAGE_BASE_URL}${process.env.LAW_OF_DEMETER_RULE_URL}`
      );
    });

    test("BlackListViolation", () => {
      const classNode: ClassNode = new ClassNode("test", range);
      const violation: BlackListViolation = new BlackListViolation(
        classNode,
        "test"
      );

      dotenv.config({ path: path.resolve(__dirname, "../../../../urls.env") });
      violation.errorMessage.should.be.equal(
        `test is on the blacklist \nidentifiers should not be comprised of words that are banned`
      );
    });
  });

  suite("Rules Test Suite", () => {
    suite("FunctionArgumentCountRule Test Suite", () => {
      test("FunctionArgumentCountRule less than 3 arguments", () => {
        ValidationManager.ruleViolations.clear();
        const methodNode: MethodNode = new MethodNode("test", 2, range);
        const rule: FunctionArgumentCountRule = new FunctionArgumentCountRule();
        rule.checkRule(methodNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("FunctionArgumentCountRule exactly 3 arguments", () => {
        ValidationManager.ruleViolations.clear();
        const methodNode: MethodNode = new MethodNode("test", 3, range);
        const rule: FunctionArgumentCountRule = new FunctionArgumentCountRule();
        rule.checkRule(methodNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("FunctionArgumentCountRule more than 3 arguments", () => {
        ValidationManager.ruleViolations.clear();
        const methodNode: MethodNode = new MethodNode("test", 4, range);
        const rule: FunctionArgumentCountRule = new FunctionArgumentCountRule();
        rule.checkRule(methodNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          FunctionArgumentCountRuleViolation
        );
      });

      test("FunctionArgumentCountRule more than 3 arguments but violation disabled", () => {
        ValidationManager.ruleViolations.clear();
        const methodNode: MethodNode = new MethodNode("test", 4, range);
        const rule: FunctionArgumentCountRule = new FunctionArgumentCountRule();
        const config: CleanCodeConfig = defaultConfig;
        config.functionArgumentCountViolation = RuleConfigLevel.off;
        rule.checkRule(methodNode, config);
        config.functionArgumentCountViolation = RuleConfigLevel.warning;

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });
    });

    suite("IdentifierNameRule Test Suite", () => {
      test("IdentifierNameRule invalid word in identifier", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAbcabcabc";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        rule.checkRule(identifierNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
      });

      test("IdentifierNameRule invalid word in identifier but naming rule violations disabled", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAbcabcabc";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.namingViolation = RuleConfigLevel.off;
        rule.checkRule(identifierNode, config);
        config.namingViolation = RuleConfigLevel.warning;

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("IdentifierNameRule invalid word in identifier first word correct type", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAbcabcabc";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        rule.checkRule(identifierNode, defaultConfig, WordType.noun, "test");

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
      });

      test("IdentifierNameRule invalid word in identifier first word incorrect type", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAbcabcabc";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        rule.checkRule(identifierNode, defaultConfig, WordType.verb, "test");

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          2
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          WordTypeRuleViolation
        );
        ValidationManager.ruleViolations.ruleViolations[1].should.be.instanceof(
          NamingRuleViolation
        );
      });

      test("IdentifierNameRule first word invalid without type", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "abcabcabcAardvarks";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        rule.checkRule(identifierNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
      });

      test("IdentifierNameRule first word invalid with type", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "abcabcabcAardvarks";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        rule.checkRule(identifierNode, defaultConfig, WordType.noun, "test");

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          WordTypeRuleViolation
        );
      });

      test("IdentifierNameRule only valid words in identifier", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        rule.checkRule(identifierNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("IdentifierNameRule only valid words in identifier, first word correct type", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        rule.checkRule(identifierNode, defaultConfig, WordType.noun, "test");

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("IdentifierNameRule only valid words in identifier, first word incorrect type", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        rule.checkRule(identifierNode, defaultConfig, WordType.verb, "test");

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          WordTypeRuleViolation
        );
      });

      test("IdentifierNameRule only valid words in identifier, first word incorrect type but word type violations disabled", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.wordTypeViolation = RuleConfigLevel.off;
        rule.checkRule(identifierNode, config, WordType.verb, "test");
        config.wordTypeViolation = RuleConfigLevel.warning;

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("IdentifierNameRule only valid words in identifier, first word on blacklist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        rule.checkRule(identifierNode, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("IdentifierNameRule only valid words in identifier, middle word on blacklist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("are");
        rule.checkRule(identifierNode, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("IdentifierNameRule only valid words in identifier, last word on blacklist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("mammals");
        rule.checkRule(identifierNode, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("IdentifierNameRule invalid word in identifier, first word on blacklist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAbcabcMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        rule.checkRule(identifierNode, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          2
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
        ValidationManager.ruleViolations.ruleViolations[1].should.be.instanceof(
          NamingRuleViolation
        );
      });

      test("IdentifierNameRule invalid word in identifier, middle word on blacklist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "AbcabcAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("are");
        rule.checkRule(identifierNode, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          2
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
        ValidationManager.ruleViolations.ruleViolations[1].should.be.instanceof(
          BlackListViolation
        );
      });

      test("IdentifierNameRule invalid word in identifier, last word on blacklist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "AbcabcAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("mammals");
        rule.checkRule(identifierNode, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          2
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
        ValidationManager.ruleViolations.ruleViolations[1].should.be.instanceof(
          BlackListViolation
        );
      });

      test("IdentifierNameRule only valid words in identifier, first word on blacklist but blacklist violations disabled", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        config.blacklistViolation = RuleConfigLevel.off;
        rule.checkRule(identifierNode, config);
        config.blacklist = [];
        config.blacklistViolation = RuleConfigLevel.warning;

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("IdentifierNameRule invalid word in identifier, but on whitelist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreAbcabc";
        const identifierNode: VariableNode = new VariableNode(
          identifier,
          range
        );
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.whitelist.push("abcabc");
        rule.checkRule(identifierNode, config);
        config.whitelist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("IdentifierNameRule identifier starts with noun instead of verb, but is on whitelist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "aardvarksAreMammals";
        const identifierNode: VariableNode = new VariableNode(identifier, range);
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.whitelist.push("aardvarks");
        rule.checkRule(identifierNode, config, WordType.verb, "method");
        config.whitelist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("IdentifierNameRule identifier starts with verb instead of noun, but is on whitelist", () => {
        ValidationManager.ruleViolations.clear();
        const identifier = "argufyAardvarks";
        const identifierNode: VariableNode = new VariableNode(identifier, range);
        const rule: IdentifierNameRule = new IdentifierNameRule(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.whitelist.push("argufy");
        rule.checkRule(identifierNode, config, WordType.noun, "class");
        config.whitelist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });
    });

    suite("LawOfDemeterRule Test Suite", () => {
      test("LawOfDemeterRule method call first part", () => {
        ValidationManager.ruleViolations.clear();
        const expressionParts: Array<ExpressionPartNode> = [];
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.methodCall,
            0,
            "test1",
            range
          )
        );
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.fieldCall,
            1,
            "test2",
            range
          )
        );
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.fieldCall,
            2,
            "test3",
            range
          )
        );
        const expressionNode: ExpressionNode = new ExpressionNode(
          expressionParts,
          range
        );
        const rule: LawOfDemeterRule = new LawOfDemeterRule();
        rule.checkRule(expressionNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("LawOfDemeterRule method call second part", () => {
        ValidationManager.ruleViolations.clear();
        const expressionParts: Array<ExpressionPartNode> = [];
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.fieldCall,
            0,
            "test1",
            range
          )
        );
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.methodCall,
            1,
            "test2",
            range
          )
        );
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.fieldCall,
            2,
            "test3",
            range
          )
        );
        const expressionNode: ExpressionNode = new ExpressionNode(
          expressionParts,
          range
        );
        const rule: LawOfDemeterRule = new LawOfDemeterRule();
        rule.checkRule(expressionNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("LawOfDemeterRule method call third part", () => {
        ValidationManager.ruleViolations.clear();
        const expressionParts: Array<ExpressionPartNode> = [];
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.fieldCall,
            0,
            "test1",
            range
          )
        );
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.fieldCall,
            1,
            "test2",
            range
          )
        );
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.methodCall,
            2,
            "test3",
            range
          )
        );
        const expressionNode: ExpressionNode = new ExpressionNode(
          expressionParts,
          range
        );
        const rule: LawOfDemeterRule = new LawOfDemeterRule();
        rule.checkRule(expressionNode, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          LawOfDemeterViolation
        );
      });

      test("LawOfDemeterRule method call third part but law of demeter violation disabled", () => {
        ValidationManager.ruleViolations.clear();
        const expressionParts: Array<ExpressionPartNode> = [];
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.fieldCall,
            0,
            "test1",
            range
          )
        );
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.fieldCall,
            1,
            "test2",
            range
          )
        );
        expressionParts.push(
          new ExpressionPartNode(
            ExpressionPartType.methodCall,
            2,
            "test3",
            range
          )
        );
        const expressionNode: ExpressionNode = new ExpressionNode(
          expressionParts,
          range
        );
        const rule: LawOfDemeterRule = new LawOfDemeterRule();
        const config: CleanCodeConfig = defaultConfig;
        config.lawOfDemeterViolation = RuleConfigLevel.off;
        rule.checkRule(expressionNode, config);
        config.lawOfDemeterViolation = RuleConfigLevel.warning;

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });
    });
  });

  suite("Validator Test Suite", () => {
    suite("ClassNameValidator Test Suite", () => {
      test("ClassNameValidator no Nodes", () => {
        ValidationManager.ruleViolations.clear();
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        validator.validate([], defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("ClassNameValidator no ClassNodes", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new MethodNode("method", 0, range),
        ];
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("ClassNameValidator no Violations", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("aardvarksAreMammals", range),
          new MethodNode("method", 0, range),
        ];
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("ClassNameValidator NamingRuleViolation", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("aardvarksAbcabcabc", range),
          new MethodNode("method", 0, range),
        ];
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
      });

      test("ClassNameValidator WordTypeRuleViolation", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("argufyAardvarks", range),
          new MethodNode("method", 0, range),
        ];
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          WordTypeRuleViolation
        );
      });

      test("ClassNameValidator BlackListViolation first word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("aardvarksAreMammals", range),
          new MethodNode("method", 0, range),
        ];
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("ClassNameValidator BlackListViolation middle word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("aardvarksAreMammals", range),
          new MethodNode("method", 0, range),
        ];
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("are");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("ClassNameValidator BlackListViolation last word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("aardvarksAreMammals", range),
          new MethodNode("method", 0, range),
        ];
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("mammals");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("ClassNameValidator all Violations", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("argufyAardvarksAbcabcabc", range),
          new MethodNode("method", 0, range),
        ];
        const validator: ClassNameValidator = new ClassNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          3
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          WordTypeRuleViolation
        );
        ValidationManager.ruleViolations.ruleViolations[1].should.be.instanceof(
          BlackListViolation
        );
        ValidationManager.ruleViolations.ruleViolations[2].should.be.instanceof(
          NamingRuleViolation
        );
      });
    });

    suite("FunctionArgumentCountValidator Test Suite", () => {
      test("FunctionArgumentCountValidator no nodes", () => {
        ValidationManager.ruleViolations.clear();
        const validator: FunctionArgumentCountValidator =
          new FunctionArgumentCountValidator();
        validator.validate([], defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("FunctionArgumentCountValidator no MethodNodes", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
        ];
        const validator: FunctionArgumentCountValidator =
          new FunctionArgumentCountValidator();
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("FunctionArgumentCountValidator less than 3 arguments", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("method", 2, range),
        ];
        const validator: FunctionArgumentCountValidator =
          new FunctionArgumentCountValidator();
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("FunctionArgumentCountValidator exactly 3 arguments", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("method", 3, range),
        ];
        const validator: FunctionArgumentCountValidator =
          new FunctionArgumentCountValidator();
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("FunctionArgumentCountValidator more than 3 arguments", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("method", 4, range),
        ];
        const validator: FunctionArgumentCountValidator =
          new FunctionArgumentCountValidator();
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          FunctionArgumentCountRuleViolation
        );
      });
    });

    suite("MethodNameValidator Test Suite", () => {
      test("MethodNameValidator no Nodes", () => {
        ValidationManager.ruleViolations.clear();
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        validator.validate([], defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("MethodNameValidator no MethodNodes", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
        ];
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("MethodNameValidator no Violations", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("argufyAardvarks", 0, range),
        ];
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("MethodNameValidator NamingRuleViolation", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("argufyAbcabcabc", 0, range),
        ];
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
      });

      test("MethodNameValidator WordTypeRuleViolation", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("aardvarksAreMammals", 0, range),
        ];
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          WordTypeRuleViolation
        );
      });

      test("MethodNameValidator BlackListViolation first word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("argufyAardvarksMammals", 0, range),
        ];
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("argufy");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("MethodNameValidator BlackListViolation middle word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("argufyAardvarksMammals", 0, range),
        ];
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("MethodNameValidator BlackListViolation last word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("argufyAardvarksMammals", 0, range),
        ];
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("mammals");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("MethodNameValidator all Violations", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("var", range),
          new ClassNode("class", range),
          new MethodNode("aardvarksAbcabcabcMammals", 0, range),
        ];
        const validator: MethodNameValidator = new MethodNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("mammals");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          3
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          WordTypeRuleViolation
        );
        ValidationManager.ruleViolations.ruleViolations[1].should.be.instanceof(
          NamingRuleViolation
        );
        ValidationManager.ruleViolations.ruleViolations[2].should.be.instanceof(
          BlackListViolation
        );
      });

      test("MethodNameValidator BlackListViolation overrides NamingRuleViolation", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new MethodNode("abcabcAreMammals", 0, range)
        ];
        const validator: MethodNameValidator = new MethodNameValidator(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("abcabc");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(BlackListViolation);
      });

      test("MethodNameValidator BlackListViolation overrides WordTypeViolation", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [new MethodNode("aardvarksAreMammals", 0, range)];
        const validator: MethodNameValidator = new MethodNameValidator(dictionary);
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceOf(BlackListViolation);
      });
    });

    suite("VariableNameValidator Test Suite", () => {
      test("VariableNameValidator no Nodes", () => {
        ValidationManager.ruleViolations.clear();
        const validator: VariableNameValidator = new VariableNameValidator(
          dictionary
        );
        validator.validate([], defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("VariableNameValidator no VariableNodes", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new ClassNode("class", range),
          new MethodNode("method", 0, range),
        ];
        const validator: VariableNameValidator = new VariableNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("VariableNameValidator no Violations", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new ClassNode("class", range),
          new MethodNode("method", 0, range),
          new VariableNode("argufyAardvarks", range),
        ];
        const validator: VariableNameValidator = new VariableNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("VariableNameValidator one NamingRuleViolation", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new ClassNode("class", range),
          new MethodNode("method", 0, range),
          new VariableNode("argufyAbcabcabc", range),
        ];
        const validator: VariableNameValidator = new VariableNameValidator(
          dictionary
        );
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
      });

      test("VariableNameValidator BlackListViolation first word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("argufyAardvarksMammals", range),
          new ClassNode("class", range),
          new MethodNode("method", 0, range),
        ];
        const validator: VariableNameValidator = new VariableNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("argufy");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("VariableNameValidator BlackListViolation middle word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("argufyAardvarksMammals", range),
          new ClassNode("class", range),
          new MethodNode("method", 0, range),
        ];
        const validator: VariableNameValidator = new VariableNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("VariableNameValidator BlackListViolation last word", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new VariableNode("argufyAardvarksMammals", range),
          new ClassNode("class", range),
          new MethodNode("method", 0, range),
        ];
        const validator: VariableNameValidator = new VariableNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("mammals");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.should.be.length(1);
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          BlackListViolation
        );
      });

      test("VariableNameValidator multiple NamingRuleViolations", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new ClassNode("class", range),
          new MethodNode("method", 0, range),
          new VariableNode("abcabcabcAardvarksAbcabcabc", range),
        ];
        const validator: VariableNameValidator = new VariableNameValidator(
          dictionary
        );
        const config: CleanCodeConfig = defaultConfig;
        config.blacklist.push("aardvarks");
        validator.validate(nodes, config);
        config.blacklist = [];

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          3
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          NamingRuleViolation
        );
        ValidationManager.ruleViolations.ruleViolations[1].should.be.instanceof(
          BlackListViolation
        );
        ValidationManager.ruleViolations.ruleViolations[2].should.be.instanceof(
          NamingRuleViolation
        );
      });
    });

    suite("LawOfDemeterValidator Test Suite", () => {
      test("LawOfDemeterValidator no Nodes", () => {
        ValidationManager.ruleViolations.clear();
        const validator: LawOfDemeterValidator = new LawOfDemeterValidator();
        validator.validate([], defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("LawOfDemeterValidator no ExpressionNode", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new ClassNode("class", range),
          new MethodNode("method", 0, range),
        ];
        const validator: LawOfDemeterValidator = new LawOfDemeterValidator();
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("LawOfDemeterValidator no Violations", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new ClassNode("class", range),
          new ExpressionNode(
            [
              new ExpressionPartNode(
                ExpressionPartType.fieldCall,
                0,
                "test1",
                range
              ),
              new ExpressionPartNode(
                ExpressionPartType.methodCall,
                1,
                "test2",
                range
              ),
            ],
            range
          ),
        ];
        const validator: LawOfDemeterValidator = new LawOfDemeterValidator();
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.should.be.empty;
      });

      test("LawOfDemeterValidator with Violation", () => {
        ValidationManager.ruleViolations.clear();
        const nodes: Array<Node> = [
          new ClassNode("class", range),
          new ExpressionNode(
            [
              new ExpressionPartNode(
                ExpressionPartType.fieldCall,
                0,
                "test1",
                range
              ),
              new ExpressionPartNode(
                ExpressionPartType.fieldCall,
                1,
                "test2",
                range
              ),
              new ExpressionPartNode(
                ExpressionPartType.methodCall,
                2,
                "test3",
                range
              ),
            ],
            range
          ),
        ];
        const validator: LawOfDemeterValidator = new LawOfDemeterValidator();
        validator.validate(nodes, defaultConfig);

        ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(
          1
        );
        ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
          LawOfDemeterViolation
        );
      });
    });
  });

  suite("ValidationManager Test Suite", () => {
    test("ValidationManager initialization", () => {
      ValidationManager.ruleViolations.should.not.be.null;
      ValidationManager.ruleViolations.should.not.be.undefined;
    });

    test("ValidationManager no Violations", () => {
      const validationManager: ValidationManager = new ValidationManager(
        dictionary
      );
      ValidationManager.ruleViolations.clear();
      const nodes: Array<Node> = [
        new ClassNode("aardvarksAreMammals", range),
        new VariableNode("aardvark", range),
        new MethodNode("argufyAardvarks", 0, range),
      ];
      validationManager.validateRules(nodes, defaultConfig);

      ValidationManager.ruleViolations.ruleViolations.should.be.empty;
    });

    test("ValidationManager ClassNameValidator has Violations", () => {
      const validationManager: ValidationManager = new ValidationManager(
        dictionary
      );
      ValidationManager.ruleViolations.clear();
      const nodes: Array<Node> = [
        new ClassNode("aardvarksAbcabcabc", range),
        new VariableNode("aardvark", range),
        new MethodNode("argufyAardvarks", 0, range),
      ];
      validationManager.validateRules(nodes, defaultConfig);

      ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(1);
      ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
        NamingRuleViolation
      );
    });

    test("ValidationManager VariableNameValidator has Violations", () => {
      const validationManager: ValidationManager = new ValidationManager(
        dictionary
      );
      ValidationManager.ruleViolations.clear();
      const nodes: Array<Node> = [
        new ClassNode("aardvarksAreMammals", range),
        new VariableNode("abcabcabc", range),
        new MethodNode("argufyAardvarks", 0, range),
      ];
      validationManager.validateRules(nodes, defaultConfig);

      ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(1);
      ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
        NamingRuleViolation
      );
    });

    test("ValidationManager MethodNameValidator has Violations", () => {
      const validationManager: ValidationManager = new ValidationManager(
        dictionary
      );
      ValidationManager.ruleViolations.clear();
      const nodes: Array<Node> = [
        new ClassNode("aardvarksAreMammals", range),
        new VariableNode("aardvark", range),
        new MethodNode("argufyAbcabcabc", 0, range),
      ];
      validationManager.validateRules(nodes, defaultConfig);

      ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(1);
      ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
        NamingRuleViolation
      );
    });

    test("ValidationManager FunctionArgumentCountValidator has Violations", () => {
      const validationManager: ValidationManager = new ValidationManager(
        dictionary
      );
      ValidationManager.ruleViolations.clear();
      const nodes: Array<Node> = [
        new ClassNode("aardvarksAreMammals", range),
        new VariableNode("aardvark", range),
        new MethodNode("argufyAardvarks", 10, range),
      ];
      validationManager.validateRules(nodes, defaultConfig);

      ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(1);
      ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
        FunctionArgumentCountRuleViolation
      );
    });

    test("ValidationManager LawOfDemeterValidator has Violations", () => {
      const validationManager: ValidationManager = new ValidationManager(
        dictionary
      );
      ValidationManager.ruleViolations.clear();
      const nodes: Array<Node> = [
        new ClassNode("aardvarksAreMammals", range),
        new VariableNode("aardvark", range),
        new MethodNode("argufyAardvarks", 1, range),
        new ExpressionNode(
          [
            new ExpressionPartNode(
              ExpressionPartType.fieldCall,
              0,
              "test1",
              range
            ),
            new ExpressionPartNode(
              ExpressionPartType.fieldCall,
              1,
              "test2",
              range
            ),
            new ExpressionPartNode(
              ExpressionPartType.methodCall,
              2,
              "test3",
              range
            ),
          ],
          range
        ),
      ];
      validationManager.validateRules(nodes, defaultConfig);

      ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(1);
      ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
        LawOfDemeterViolation
      );
    });

    test("ValidationManager multiple Validators have Violations", () => {
      const validationManager: ValidationManager = new ValidationManager(
        dictionary
      );
      ValidationManager.ruleViolations.clear();
      const nodes: Array<Node> = [
        new ClassNode("aardvarksAbcabcabc", range),
        new VariableNode("abcabcabc", range),
        new MethodNode("argufyAbcabcabc", 10, range),
        new ExpressionNode(
          [
            new ExpressionPartNode(
              ExpressionPartType.fieldCall,
              0,
              "test1",
              range
            ),
            new ExpressionPartNode(
              ExpressionPartType.fieldCall,
              1,
              "test2",
              range
            ),
            new ExpressionPartNode(
              ExpressionPartType.methodCall,
              2,
              "test3",
              range
            ),
          ],
          range
        ),
      ];
      const config: CleanCodeConfig = defaultConfig;
      config.blacklist.push("aardvarks");
      validationManager.validateRules(nodes, config);
      config.blacklist = [];

      ValidationManager.ruleViolations.ruleViolations.length.should.be.equal(6);
      ValidationManager.ruleViolations.ruleViolations[0].should.be.instanceof(
        BlackListViolation
      );
      ValidationManager.ruleViolations.ruleViolations[1].should.be.instanceof(
        NamingRuleViolation
      );
      ValidationManager.ruleViolations.ruleViolations[2].should.be.instanceof(
        NamingRuleViolation
      );
      ValidationManager.ruleViolations.ruleViolations[3].should.be.instanceof(
        NamingRuleViolation
      );
      ValidationManager.ruleViolations.ruleViolations[4].should.be.instanceof(
        FunctionArgumentCountRuleViolation
      );
      ValidationManager.ruleViolations.ruleViolations[5].should.be.instanceof(
        LawOfDemeterViolation
      );
    });

    test("ValidationManager Violations are cleared before next Validation", () => {
      const validationManager: ValidationManager = new ValidationManager(
        dictionary
      );
      ValidationManager.ruleViolations.clear();
      let nodes: Array<Node> = [
        new ClassNode("aardvarksAbcabcabc", range),
        new VariableNode("abcabcabc", range),
        new MethodNode("argufyAbcabcabc", 10, range),
      ];
      validationManager.validateRules(nodes, defaultConfig);

      nodes = [
        new ClassNode("aardvarksAreMammals", range),
        new VariableNode("aardvark", range),
        new MethodNode("argufyAardvarks", 0, range),
      ];
      validationManager.validateRules(nodes, defaultConfig);

      ValidationManager.ruleViolations.ruleViolations.should.be.empty;
    });
  });
});

const range: Range = Range.create(
  Position.create(0, 0),
  Position.create(0, 10)
);
