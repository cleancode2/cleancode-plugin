import { CleanCodeConfig } from "../../clean-code-config";
import { MethodNode, Node } from "../../parser/parser/nodes/_index";
import { Dictionary } from "../../resources/dictionary";
import { WordType } from "../helper/identifier-helper";
import { IdentifierNameRule } from "../rules/_index";
import { Validator } from "./validator";

export class MethodNameValidator implements Validator {
  private identifierNameRule: IdentifierNameRule;

  constructor(dictionary: Dictionary) {
    this.identifierNameRule = new IdentifierNameRule(dictionary);
  }

  validate(nodes: Array<Node>, settings: CleanCodeConfig): void {
    nodes.forEach((node) => {
      if (node instanceof MethodNode) {
        this.identifierNameRule.checkRule(
          node,
          settings,
          WordType.verb,
          "method"
        );
      }
    });
  }
}
