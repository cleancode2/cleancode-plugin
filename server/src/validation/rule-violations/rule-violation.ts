import { Node } from "../../parser/parser/nodes/_index";

export abstract class RuleViolation {
  private _node: Node;
  private _errorMessage: string;

  constructor(node: Node, errorMessage: string) {
    this._node = node;
    this._errorMessage = errorMessage;
  }

  get node(): Node {
    return this._node;
  }

  get errorMessage(): string {
    return this._errorMessage;
  }
}
