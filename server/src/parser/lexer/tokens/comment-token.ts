import { Range } from "vscode-languageserver";
import { Token } from "./token";

export class CommentToken extends Token {
  private _comment: string;

  constructor(comment: string, range: Range) {
    super(range);
    this._comment = comment;
  }

  get comment(): string {
    return this._comment;
  }
}
