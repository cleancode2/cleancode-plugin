import { Range, TextDocument } from "vscode-languageserver-textdocument";
import { ParseManager } from "../../parser/parse-manager";
import {
  ClassNode,
  MethodNode,
  Node,
  VariableNode,
} from "../../parser/parser/nodes/_index";
import chai from "chai";
import { RangeHelper } from "../../parser/lexer/helper/range-helper";
import { AssertionHelper } from "../helper/assertion-helper";
import { Reader } from "../../parser/lexer/helper/reader";
chai.should();

suite("Parser Test Suite", () => {
  suite("ParseManager", () => {
    test("constructor language recognized", () => {
      const document: TextDocument = getTextDocument(
        "java",
        "public class A { }"
      );
      const parseManager: ParseManager = new ParseManager(document.languageId);
      const nodes: Array<Node> = parseManager.parseDocument(document);
      nodes.length.should.equal(1);
    });

    test("constructor langauge recognized parser not yet implemented", () => {
      const document: TextDocument = getTextDocument(
        "csharp",
        "public class A { }"
      );
      const parseManager: ParseManager = new ParseManager(document.languageId);
      const nodes: Array<Node> = parseManager.parseDocument(document);
      nodes.should.be.empty;
    });

    test("constructor language not recognized", () => {
      const document: TextDocument = getTextDocument(
        "c++",
        "public class A { }"
      );
      const parseManager: ParseManager = new ParseManager(document.languageId);
      const nodes: Array<Node> = parseManager.parseDocument(document);
      nodes.should.be.empty;
    });

    test("parseDocument", () => {
      const document: TextDocument = getTextDocument(
        "java",
        "public class A { int x = 1; int get() { return x; } }"
      );
      const parseManager: ParseManager = new ParseManager(document.languageId);
      const nodes: Array<Node> = parseManager.parseDocument(document);
      nodes.should.be.length(3);
      nodes[0].should.be.instanceof(ClassNode);
      nodes[1].should.be.instanceof(VariableNode);
      nodes[2].should.be.instanceof(MethodNode);
    });
  });

  suite("RangeHelper", () => {
    test("get range with default values", () => {
      const rangeHelper: RangeHelper = new RangeHelper();
      AssertionHelper.assertRange(rangeHelper.range, 0, -1, 0, -1);
    });

    test("setStart", () => {
      const rangeHelper: RangeHelper = new RangeHelper();
      const lineStart = 10;
      const lineEnd = 15;
      const charStart = 0;
      const charEnd = 20;
      rangeHelper.setStart(lineStart, lineEnd, charStart, charEnd);
      AssertionHelper.assertRange(
        rangeHelper.range,
        lineStart,
        charStart,
        lineEnd,
        charEnd
      );
    });

    test("setEnd", () => {
      const rangeHelper: RangeHelper = new RangeHelper();
      const lineStart = 10;
      const lineEnd = 15;
      const charStart = 0;
      const charEnd = 20;
      rangeHelper.setStart(lineStart, lineEnd, charStart, charEnd);

      const newLineEnd = 25;
      const newCharEnd = 5;
      rangeHelper.setEnd(newLineEnd, newCharEnd);
      AssertionHelper.assertRange(
        rangeHelper.range,
        lineStart,
        charStart,
        newLineEnd,
        newCharEnd
      );
    });
  });

  suite("Reader", () => {
    suite("with undefined text", () => {
      test("getCurrent", () => {
        const reader: Reader = new Reader();
        const current = reader.getCurrent();
        current.should.be.equal("");
      });

      test("next", () => {
        const reader: Reader = new Reader();
        (function () {
          reader.next();
        }.should.not.throw(Error));
      });

      test("skipBlanks", () => {
        const reader: Reader = new Reader();
        (function () {
          reader.skipBlanks();
        }.should.not.throw(Error));
      });

      test("isEnd", () => {
        const reader: Reader = new Reader();
        const end = reader.isEnd();
        end.should.be.true;
      });

      test("setStartIndex", () => {
        const reader: Reader = new Reader();
        (function () {
          reader.setStartIndex();
        }.should.not.throw(Error));
      });

      test("setEndIndex test", () => {
        const reader: Reader = new Reader();
        (function () {
          reader.setEndIndex();
        }.should.not.throw(Error));
      });

      test("getRange test", () => {
        const reader: Reader = new Reader();
        const range: Range = reader.getRange();
        AssertionHelper.assertRange(range, 0, -1, 0, -1);
      });
    });

    suite("with text", () => {
      test("getCurrent from start", () => {
        const reader: Reader = new Reader();
        const document: TextDocument = getTextDocument("java", "class A { }");
        reader.setText(document);
        const current: string = reader.getCurrent();
        current.should.be.equal("c");
      });

      test("next", () => {
        const reader: Reader = new Reader();
        const document: TextDocument = getTextDocument("java", "class A { }");
        reader.setText(document);
        for (let i = 0; i < 8; i++) {
          reader.next();
        }
        const current: string = reader.getCurrent();
        current.should.be.equal("{");
      });

      test("next with line breaks", () => {
        const reader: Reader = new Reader();
        const document: TextDocument = getTextDocument(
          "java",
          "class A {\n" + "  int x = 1;\n" + "}"
        );
        reader.setText(document);
        for (let i = 0; i < 12; i++) {
          reader.next();
        }
        const current: string = reader.getCurrent();
        current.should.be.equal("i");
      });

      test("skipBlanks", () => {
        const reader: Reader = new Reader();
        const document: TextDocument = getTextDocument(
          "java",
          "        class A { }"
        );
        reader.setText(document);
        let current: string = reader.getCurrent();

        current.should.be.equal(" ");
        reader.skipBlanks();
        current = reader.getCurrent();
        current.should.be.equal("c");
      });

      test("isEnd", () => {
        const reader: Reader = new Reader();
        const document: TextDocument = getTextDocument("java", "class A { }");
        reader.setText(document);
        let counter = 0;
        while (!reader.isEnd()) {
          reader.next();
          counter++;
        }

        counter.should.be.equal(10);
      });

      test("setStartIndex", () => {
        const reader: Reader = new Reader();
        const document: TextDocument = getTextDocument(
          "java",
          "class Test { }"
        );
        reader.setText(document);

        while (reader.getCurrent() !== "T") {
          reader.next();
        }
        reader.setStartIndex();
        const range: Range = reader.getRange();

        AssertionHelper.assertRange(range, 0, 6, 0, 6);
      });

      test("setEndIndex", () => {
        const reader: Reader = new Reader();
        const document: TextDocument = getTextDocument(
          "java",
          "class Test { }"
        );
        reader.setText(document);

        while (reader.getCurrent() !== "T") {
          reader.next();
        }
        reader.setStartIndex();
        reader.next();
        while (reader.getCurrent() !== "t") {
          reader.next();
        }
        reader.setEndIndex();
        const range: Range = reader.getRange();

        AssertionHelper.assertRange(range, 0, 6, 0, 9);
      });
    });
  });
});

function getTextDocument(languageId: string, content: string): TextDocument {
  return TextDocument.create("file://test", languageId, 1, content);
}
