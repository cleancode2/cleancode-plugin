import { Range } from "vscode-languageserver";
import { IdentifierNode } from "./_index";

export class MethodNode extends IdentifierNode {
  private _argumentCount: number;
  constructor(name: string, argumentCount: number, range: Range) {
    super(name, range);
    this._argumentCount = argumentCount;
  }

  get argumentCount(): number {
    return this._argumentCount;
  }
}
