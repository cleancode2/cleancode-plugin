import { TextDocument } from "vscode-languageserver-textdocument";
import { JavaParser } from "./parser/implementations/java/java-parser";
import { Node } from "./parser/nodes/_index";
import { Parser } from "./parser/parser";

export class ParseManager {
  private parser?: Parser;

  constructor(language: string) {
    this.parser = this.getParser(language);
  }

  parseDocument(document: TextDocument): Array<Node> {
    return this.parser?.parse(document) ?? [];
  }

  private getParser(language: string): Parser | undefined {
    switch (language) {
      case "csharp":
        return undefined;
      case "java":
        return new JavaParser();
      default:
        return undefined;
    }
  }
}
