\hypertarget{section-introduction-and-goals}{%
  \section{Introduction and Goals}\label{section-introduction-and-goals}}

This section describes the essential requirements for and driving forces behind the development of the architecture and implementation of the system.

\hypertarget{task}{%
  \subsection{Task}\label{task}}

This clean code extension was initially developed in the course of a student research project~\footnote{https://eprints.ost.ch/id/eprint/1005/1/HS\%202021\%202022-SA-EP-Schneider-Fuhrer-Clean\%20Code\%20Plugin.pdf} and extended as a bachelor thesis at the Eastern Switzerland University of Applied Sciences. The following section provides the complete task description translated from German.

\hypertarget{problem-description}{%
  \subsection{Problem description}\label{problem-description}}

There exist numerous recommendations how one can write good and comprehensible program code. These recommendations are often grouped under the term ``clean code'', which was coined by Martin Fowler. The recommendations concern naming variables or functions with meaningful names following a consistent scheme, for example. In pratice these rules are not always followed. One approach to rectify this would be to notify developers every time they violate a rule. This could be achieved through an IDE plugin (i.e. VSCode) or a linter extension (i.e. ESLint). Linters can check adherence to some rules. For many rules however a deeper analysis of the source code is required, for example adherence to a consistent naming scheme. For this example the system has to disassemble the identifiers into their relevant semantic parts (i.e. \texttt{``isActive''} to \texttt{``is''} and \texttt{``active''}).

\hypertarget{requirements}{%
  \subsection{Requirements}\label{requirements}}

Due to the simplicity of the use cases, we decided against drawing use case diagrams. Instead we describe the requirements through a mix of user stories and MUST/CAN requirements. They are listed in table~\ref{ex:table-functional-requirements}.

\hypertarget{programmer}{%
  \subsubsection{Programmer}\label{programmer}}

\textit{As a developer I want to be able to activate the extension in my IDE for it to highlight all problematic parts in my source code.}

\begin{table}[!th]
  \centering
  \caption{Functional Requirements}
  \begin{tabular}{llp{85mm}}
    \toprule
    \textbf{ID} & \textbf{Priority} & \textbf{Description}                                                                                     \\
    \midrule
    FA-1        & MUST              & The extension checks the document for rule violations on save and marks them.                            \\
    \midrule
    FA-2        & MUST              & All rule violations are displayed in the IDE problem log, including their position in the document.      \\
    \midrule
    FA-3        & MUST              & The extension is easy to add to the IDE through its marketplace (no manual installation required).       \\
    \midrule
    FA-4        & MUST              & The problem message contains a weblink for further information and explanations about the violated rule. \\
    \bottomrule
  \end{tabular}\label{ex:table-functional-requirements}
\end{table}

\FloatBarrier{}

\hypertarget{quality-goals}{%
  \subsubsection{Quality Goals}\label{quality-goals}}

The quality goals are statet in the form of non functional requirements, split into MUST and CAN requirements. They are listed in table~\ref{ex:table-non-functional-requirements}.

\begin{table}[!th]
  \centering
  \caption{Non Functional Requirements}
  \begin{tabular}{llp{85mm}}
    \toprule
    \textbf{ID} & \textbf{Priority} & \textbf{Description}                                                                                           \\
    \midrule
    NF-1        & CAN               & Rule validation runs in its own thread and does not block the IDE\@.                                           \\
    \midrule
    NF-2        & MUST              & The extension works as intended even with large files (>1000 lines of code).                                   \\
    \midrule
    NF-3        & MUST              & The extension is easily extensible with additional rules and support for other programming languages.          \\
    \midrule
    NF-4        & MUST              & The source code of the extension has a rating of A in SonarQube.                                               \\
    \midrule
    NF-5        & CAN               & Unit test coverage exceeds 80\%.                                                                               \\
    \midrule
    NF-6        & CAN               & Developer that don't yet know the source code, can find relevant parts within 30 minutes when fixing problems. \\
    \midrule
    NF-7        & MUST              & The extension is published as an open source project including guidelines for contribution.                    \\
    \bottomrule
  \end{tabular}\label{ex:table-non-functional-requirements}
\end{table}

\pagebreak

\hypertarget{stakeholders}{%
  \subsubsection{Stakeholders}\label{stakeholders}}

The stakeholders of this project are listed in table~\ref{ex:table-stakeholders} including their role, name and expectations concerning architecture and documentation.

\begin{table}[!th]
  \centering
  \caption{Stakeholders}
  \begin{tabular}{llp{64mm}}
    \toprule
    \textbf{Role}            & \textbf{Name}               & \textbf{Expectation}                                            \\
    \midrule
    Advisor                  & Prof.\ Frieder Loch         & Knowledge gain; Good result which can be extended in the future \\
    \midrule
    \makecell[tl]{Developer} & \makecell[tl]{Rafael Fuhrer                                                                   \\ Pascal Schneider} & \makecell[tl]{Good result; Learning new technologies;\\ Clean implementation and \\ documentation}\\
    \bottomrule
  \end{tabular}\label{ex:table-stakeholders}
\end{table}
