import { Range } from "vscode-languageserver";
import { Tag } from "../helper/tag";
import { Token } from "./token";

export class StaticToken extends Token {
  private _tag: Tag;

  constructor(tag: Tag, range: Range) {
    super(range);
    this._tag = tag;
  }

  get tag(): Tag {
    return this._tag;
  }
}
