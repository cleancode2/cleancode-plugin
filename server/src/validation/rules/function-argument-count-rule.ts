import { CleanCodeConfig, RuleConfigLevel } from "../../clean-code-config";
import { MethodNode } from "../../parser/parser/nodes/_index";
import { FunctionArgumentCountRuleViolation } from "../rule-violations/_index";
import { ValidationManager } from "../validation-manager";
import { Rule } from "./rule";

export class FunctionArgumentCountRule implements Rule {
  checkRule(node: MethodNode, settings: CleanCodeConfig): void {
    if (settings.functionArgumentCountViolation !== RuleConfigLevel.off) {
      if (node.argumentCount > 3) {
        ValidationManager.ruleViolations.addViolation(
          new FunctionArgumentCountRuleViolation(node)
        );
      }
    }
  }
}
