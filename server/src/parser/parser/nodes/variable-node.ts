import { Range } from "vscode-languageserver";
import { IdentifierNode } from "./_index";

export class VariableNode extends IdentifierNode {
  constructor(name: string, range: Range) {
    super(name, range);
  }
}
