export interface CleanCodeConfig {
  maxNumberOfProblems: number;
  blacklist: Array<string>;
  whitelist: Array<string>;
  blacklistViolation: RuleConfigLevel;
  functionArgumentCountViolation: RuleConfigLevel;
  lawOfDemeterViolation: RuleConfigLevel;
  namingViolation: RuleConfigLevel;
  wordTypeViolation: RuleConfigLevel;
}

export enum RuleConfigLevel {
  "off" = "off",
  "warning" = "warning",
  "error" = "error",
}