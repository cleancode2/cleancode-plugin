import { CleanCodeConfig } from "../../clean-code-config";
import { ExpressionNode, Node } from "../../parser/parser/nodes/_index";
import { LawOfDemeterRule } from "../rules/_index";
import { Validator } from "./_index";

export class LawOfDemeterValidator implements Validator {
  private lawOfDemeterRule: LawOfDemeterRule;

  constructor() {
    this.lawOfDemeterRule = new LawOfDemeterRule();
  }

  validate(nodes: Array<Node>, settings: CleanCodeConfig): void {
    nodes.forEach((node) => {
      if (node instanceof ExpressionNode) {
        this.lawOfDemeterRule.checkRule(node, settings);
      }
    });
  }
}
