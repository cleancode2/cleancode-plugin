import { Range } from "vscode-languageserver-textdocument";
import chai from "chai";
import {
  ClassNode,
  ExpressionNode,
  ExpressionPartType,
  MethodNode,
  Node,
  VariableNode,
} from "../../parser/parser/nodes/_index";
chai.should();

export class AssertionHelper {
  public static assertRange(
    range: Range,
    startLine: number,
    startCharacter: number,
    endLine: number,
    endCharacter: number
  ): void {
    range.start.line.should.equal(startLine);
    range.start.character.should.equal(startCharacter);
    range.end.line.should.equal(endLine);
    range.end.character.should.equal(endCharacter);
  }

  public static assertClassNode(
    node: Node,
    name: string,
    startLine: number,
    startCharacter: number,
    endLine: number,
    endCharacter: number
  ): void {
    node.should.be.instanceof(ClassNode);
    const classNode: ClassNode = node as ClassNode;
    classNode.name.should.equal(name);
    this.assertRange(
      classNode.range,
      startLine,
      startCharacter,
      endLine,
      endCharacter
    );
  }

  public static assertVariableNode(
    node: Node,
    name: string,
    startLine: number,
    startCharacter: number,
    endLine: number,
    endCharacter: number
  ): void {
    node.should.be.instanceof(VariableNode);
    const classNode: VariableNode = node as VariableNode;
    classNode.name.should.equal(name);
    this.assertRange(
      classNode.range,
      startLine,
      startCharacter,
      endLine,
      endCharacter
    );
  }

  public static assertMethodNode(
    node: Node,
    numberOfArguments: number,
    name: string,
    startLine: number,
    startCharacter: number,
    endLine: number,
    endCharacter: number
  ): void {
    node.should.be.instanceof(MethodNode);
    const methodNode: MethodNode = node as MethodNode;
    methodNode.argumentCount.should.equal(numberOfArguments);
    methodNode.name.should.equal(name);
    AssertionHelper.assertRange(
      methodNode.range,
      startLine,
      startCharacter,
      endLine,
      endCharacter
    );
  }

  public static assertExpressionNode(
    node: Node,
    types: Array<ExpressionPartType>,
    names: Array<string>,
    startLine: number,
    startCharacter: number,
    endLine: number,
    endCharacter: number
  ): void {
    node.should.be.instanceof(ExpressionNode);
    const expressionNode: ExpressionNode = node as ExpressionNode;
    expressionNode.parts.length.should.be.equal(types.length);
    for (let i = 0; i < types.length; i++) {
      expressionNode.parts[i].order.should.be.equal(i + 1);
      expressionNode.parts[i].type.should.be.equal(types[i]);
      expressionNode.parts[i].name.should.be.equal(names[i]);
    }
    AssertionHelper.assertRange(
      expressionNode.range,
      startLine,
      startCharacter,
      endLine,
      endCharacter
    );
  }
}
