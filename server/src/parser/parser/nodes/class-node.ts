import { Range } from "vscode-languageserver";
import { IdentifierNode } from "./_index";

export class ClassNode extends IdentifierNode {
  constructor(name: string, range: Range) {
    super(name, range);
  }
}
