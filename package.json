{
  "name": "cleancode",
  "description": "Visual Studio Code Plugin that checks source code against cleancode principles as laid out by Robert C. Martin.",
  "icon": "docs/images/logo.png",
  "author": "Rafael Fuhrer, Pascal Schneider",
  "license": "Apache 2",
  "version": "1.0.0",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/cleancode2/cleancode-plugin"
  },
  "publisher": "fuhrer-schneider",
  "categories": [
    "Education"
  ],
  "keywords": [
    "cleancode",
    "Clean Code"
  ],
  "engines": {
    "vscode": "^1.63.0"
  },
  "activationEvents": [
    "onLanguage:java"
  ],
  "main": "./client/out/extension",
  "contributes": {
    "configuration": {
      "type": "object",
      "title": "Clean Code Configuration",
      "properties": {
        "cc-language-server.maxNumberOfProblems": {
          "scope": "resource",
          "type": "number",
          "default": 100,
          "description": "Controls the maximum number of problems produced by the server."
        },
        "cc-language-server.trace.server": {
          "scope": "window",
          "type": "string",
          "enum": [
            "off",
            "messages",
            "verbose"
          ],
          "default": "off",
          "description": "Traces the communication between VS Code and the language server."
        },
        "cc-language-server.blacklist": {
          "scope": "resource",
          "type": "array",
          "default": [
            "manager"
          ],
          "description": "Remove words from default dictionary"
        },
        "cc-language-server.whitelist": {
          "scope": "resource",
          "type": "array",
          "default": [
            "api",
            "rest"
          ],
          "description": "Add words to default dictionary"
        },
        "cc-language-server.violations.blacklistViolation": {
          "scope": "resource",
          "type": "string",
          "enum": [
            "off",
            "warning",
            "error"
          ],
          "default": "warning",
          "description": "Manages severity of violations against the blacklist."
        },
        "cc-language-server.violations.functionArgumentCountViolation": {
          "scope": "resource",
          "type": "string",
          "enum": [
            "off",
            "warning",
            "error"
          ],
          "default": "warning",
          "description": "Manages severity of violations of functions that have more than 3 arguments."
        },
        "cc-language-server.violations.lawOfDemeterViolation": {
          "scope": "resource",
          "type": "string",
          "enum": [
            "off",
            "warning",
            "error"
          ],
          "default": "warning",
          "description": "Manages severity of violations against the law of demeter."
        },
        "cc-language-server.violations.namingViolation": {
          "scope": "resource",
          "type": "string",
          "enum": [
            "off",
            "warning",
            "error"
          ],
          "default": "warning",
          "description": "Manages severity of violations of identifiers not being comprised of full words."
        },
        "cc-language-server.violations.wordTypeViolation": {
          "scope": "resource",
          "type": "string",
          "enum": [
            "off",
            "warning",
            "error"
          ],
          "default": "warning",
          "description": "Manages severity of violations of identifiers being of the wrong word type (class not starting with a noun or function not starting with a verb)."
        }
      }
    },
    "menus": {
      "editor/context": [
        {
          "when": "resourceLangId == java",
          "submenu": "cleancode",
          "group": "navigation"
        }
      ],
      "cleancode": [
        {
          "command": "cleancode.addToWhitelist",
          "group": "navigation"
        },
        {
          "command": "cleancode.addToBlacklist",
          "group": "navigation"
        }
      ]
    },
    "submenus": [
      {
        "id": "cleancode",
        "label": "CleanCode Extension"
      }
    ],
    "commands": [
      {
        "command": "cleancode.addToWhitelist",
        "title": "Add to Whitelist"
      },
      {
        "command": "cleancode.addToBlacklist",
        "title": "Add to Blacklist"
      }
    ]
  },
  "scripts": {
    "vscode:prepublish": "npm run compile",
    "compile": "tsc -b",
    "watch": "tsc -b -w",
    "lint": "eslint ./client/src ./server/src --ext .ts,.tsx",
    "postinstall": "cd client && npm install && cd ../server && npm install && cd ..",
    "test": "nyc mocha",
    "deploy": "vsce publish --no-yarn"
  },
  "devDependencies": {
    "@istanbuljs/nyc-config-typescript": "^1.0.2",
    "@types/chai": "^4.3.0",
    "@types/glob": "^7.1.4",
    "@types/mocha": "^9.1.0",
    "@types/node": "^14.17.0",
    "@typescript-eslint/eslint-plugin": "^5.15.0",
    "@typescript-eslint/parser": "^5.15.0",
    "ajv": "^8.8.2",
    "chai": "^4.3.6",
    "eslint": "^8.11.0",
    "glob": "^7.1.7",
    "mocha": "^9.2.2",
    "nyc": "^15.1.0",
    "source-map-support": "^0.5.21",
    "ts-node": "^9.1.1",
    "typescript": "^4.6.2",
    "vsce": "^2.9.1"
  }
}
