# Benennung von Variablen und Funktionen

## Zusammenfassung

Die Verwendung von klaren und verständlichen Bezeichnern für Variablen und Funktionen helfen, Code für alle verständlicher zu machen. Je beschreibender ein Bezeichner ist, desto besser. So sollten Klassennamen mit einem Nomen beginnen und Funktionen mit einem Verb. Jegliche Art von Kodierung sollte vermieden werden. Die Anwendung dieser Prinzipien verbessert auch die Suchbarkeit, was es anderen Programmierenden ebenfalls vereinfacht den Code schnell zu verstehen.

### Problembeschreibung

#### Klare Namen

Namen sollten aussprechbar sein, damit unser Gehirn einfacher mit ihnen umgehen kann. Damit ist insbesondere die Vermeidung von wenig bekannten Akronymen & Abkürzungen aber auch das Weglassen einzelner Buchstaben eines Wortes gemeint. Denn diese Gegebenheiten fuhren dazu, dass unser Gehirn mehr Schwierigkeiten hat den Quelltext “flüssig” zu verarbeiten.

Codierungen sollten aus den selben Gründen ebenfalls vermieden werden, da das zusätzliche Entschlüsseln dieser Codierung das Lesen und Interpretieren weiter erschwert. Das gleiche gilt für eigene (unternehmensinterne) Codierungen.

#### Suchbarkeit

Es kann beim Programmieren (vor allem mit grösseren Codebasen) nötig sein, dass man nach einer bestimmten Variablen oder Funktion suchen muss, deren Namen man nicht genau kennt. Hier hilft es wenn bezeichnende, vollständige Namen (also ohne Abkürzungen, Akronyme etc..) verwendet wurden.


## Lösung
Aussprechbare und Suchbare Namen müssen immer aus vollständigen Wörtern zusammengesetzt sein. Sämtliche Codierungen sind zu vermeiden. Ausserdem sollten nach Möglichkeit möglichst bezeichnende Namen verwendet werden, da dies die Suchbarkeit weiter erhöht.

## Code Beispiele

### Positive Beispiele

Bei den folgenden Beispielen handelt es sich um Variablen und Funktionen mit sinnvoller, klarer Benennungen die den Anforderungen dieser Regel entsprechen.

    class Triangle(...){...}
    
    print(...){...}

    class Customer(...){...}

    getCustomer(...){...}

Alle Namen setzen sich aus vollständigen Wörter zusammen, enthalten keine Codierungen und sind somit suchbar.

### Negative Beispiele

Die folgenden Beispiele verstossen gegen die Benennungsregel.

Klare Namen:

    class DtaRcrd {...}

Dieser Klassennamen ist codiert und setzt sich somit aus nicht vollständigen Wörtern zusammen. Somit ist er auch kaum suchbar. Etwas besser wäre:

    class DataRecord{...}

Nun setzt sich der Name aus vollständigen Wörtern zusammen, verzichtet auf Codierungen und ist somit eher suchbar. Leider ist ein DataRecord wenig spezifisch. Jede Klasse ist am Ende ein DataRecord. Unter der Annahme das es sich beim DataRecord um die Daten eines Kunden handelt wäre eine gute Lösung daher:

    class Costumer{...}

Codierungen:

    public class Part {
        private String m_dsc: \\ die textliche Beschreibung
        void setName(String name) {
            m_dsc = name;
        }
    }

Dieser Codeausschnitt ist nicht schnell und intuitiv verständlich. Deutlich besser wäre:

    public class Part {
        String description;
        void setDescription(String description) {
            this.description = description;
        }
    }

## Vorteile der Regel

Die folgenden Vorteile entstehen bei der Einhaltung dieser Regel:

- Code kann einfacher gelesen & verstanden werden
- Wenn der Code verändert werden soll, findet man die entsprechenden Variablen / Funktionen schneller
- Der Zweck von Variablen und Funktionen ist für alle die den Code lesen intuitiv ersichtlich

## Nachteile der Regel

Die folgenden Nachteile entstehen bei der Einhaltung dieser Regel:

- Höherer initialer Aufwand beim Entwickeln
- Mehr "typing" nötig (die meisten IDE's verfügen heute aber über auto-complete Funktionen)

## FAQ

### Warum sollte ich keine "allgemein bekannten" wie z.B. (Desc für Description) Abkürzungen verwenden?

Antwort: Weil auch allgemein bekannte Abkürzungen nicht zwingend jedem Entwickler im gleichen Masse bekannt sind. Für das menschliche Gehirn ist es immer schwerer mit Codierungen zu arbeiten, als mit vollständig ausgeschriebenen Namen. 


### Gibt es Fälle wo diese Regel nicht eingehalten werden soll / kann?

Antwort: Ja, auf Systemen auf denen extrem wenig Speicher zur Verfügung steht und der Quellcode entsprechend gekürzt werden muss. Es gibt ausserdem Systeme bei denen alle möglichen Variablen vorgegeben sind.
