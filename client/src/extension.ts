import * as path from "path";
import { TextEncoder } from "util";
import {
  commands,
  ExtensionContext,
  Position,
  TextDocument,
  Uri,
  window,
  workspace,
} from "vscode";
import {
  LanguageClient,
  LanguageClientOptions,
  ServerOptions,
  TransportKind,
} from "vscode-languageclient/node";
import { CleanCodeConfig } from "./clean-code-config";

let client: LanguageClient;

export async function activate(context: ExtensionContext) {
  commands.registerCommand("cleancode.addToWhitelist", async () => {
    await addToWhitelist();
  });

  commands.registerCommand("cleancode.addToBlacklist", async () => {
    await addToBlacklist();
  });

  const serverModule = context.asAbsolutePath(
    path.join("server", "out", "server.js")
  );

  // --inspect=6009: runs the server in Node's Inspector mode so VS Code can attach to the server for debugging
  const debugOptions = { execArgv: ["--nolazy", "--inspect=6009"] };

  const serverOptions: ServerOptions = {
    run: { module: serverModule, transport: TransportKind.ipc },
    debug: {
      module: serverModule,
      transport: TransportKind.ipc,
      options: debugOptions,
    },
  };

  const clientOptions: LanguageClientOptions = {
    documentSelector: [{ scheme: "file", language: "java" }],
    synchronize: {
      fileEvents: workspace.createFileSystemWatcher(
        "**/cleancodeconfig.json",
        false,
        false,
        false
      ),
    },
  };

  client = new LanguageClient(
    "cc-language-server",
    "Cleancode Extension Langage Server",
    serverOptions,
    clientOptions
  );

  client.start();

  client.onReady().then(() => {
    client.onRequest("getFile", (name: string): Promise<string> => {
      return getFile(name);
    });
  });
}

export function deactivate(): Thenable<void> | undefined {
  if (!client) {
    return undefined;
  }
  return client.stop();
}

async function getFile(name: string): Promise<string> {
  const uri: Uri = await getFileUri(name);
  let text: string = undefined;

  if (uri !== undefined) {
    let textDocument: TextDocument;
    await workspace
      .openTextDocument(uri)
      .then((value) => (textDocument = value));
    text = textDocument.getText();
  }

  return text;
}

async function getFileUri(name: string): Promise<Uri> {
  let uri: Uri = undefined;

  await workspace.findFiles(name, null, 1).then((value) => {
    if (value.length) {
      uri = value[0];
    }
  });

  return uri;
}

async function addToWhitelist(): Promise<void> {
  const words = await getSelectedWords();
  const uri = await getFileUri("cleancodeconfig.json");

  if (uri !== undefined) {
    const configFile = await getConfigFile(uri);
    configFile.whitelist.push(...words);
    await updateConfigFile(uri, configFile);
  } else {
    await updateConfiguration("whitelist", words);
  }
}

async function addToBlacklist(): Promise<void> {
  const words = await getSelectedWords();
  const uri = await getFileUri("cleancodeconfig.json");

  if (uri !== undefined) {
    const configFile = await getConfigFile(uri);
    configFile.blacklist.push(...words);
    await updateConfigFile(uri, configFile);
  } else {
    await updateConfiguration("blacklist", words);
  }
}

async function getSelectedWords(): Promise<Array<string>> {
  const selections = window.activeTextEditor.selections;

  const words: Array<string> = [];
  const document = window.activeTextEditor.document;
  for (const selection of selections) {
    const position = new Position(
      selection.start.line,
      selection.start.character
    );
    const word = document.getText(document.getWordRangeAtPosition(position));
    const identifierParts = await identifierToWords(word);
    words.push(...identifierParts);
  }

  return words;
}

async function updateConfiguration(section: string, value: any): Promise<void> {
  const config = workspace.getConfiguration("cc-language-server");
  const whitelist: Array<string> = config.get(section);
  whitelist.push(...value);
  config.update(section, whitelist, false);
}

async function getConfigFile(uri: Uri): Promise<CleanCodeConfig> {
  let textDocument: TextDocument;
  await workspace.openTextDocument(uri).then((value) => (textDocument = value));
  const configFile: CleanCodeConfig = JSON.parse(textDocument.getText());
  return configFile;
}

async function updateConfigFile(
  uri: Uri,
  configFile: CleanCodeConfig
): Promise<void> {
  await workspace.fs.writeFile(
    uri,
    new TextEncoder().encode(JSON.stringify(configFile))
  );
}

async function identifierToWords(identifier: string): Promise<Array<string>> {
  return (
    identifier
      // replaces numbers with blanks
      .replace(/(\d+)/g, " ")
      // adds blank between lowercase letter followerd by uppercase letter
      .replace(/([a-z]+)([A-Z])/g, "$1 $2")
      // adds blank before last uppercase letter, if there are multiple uppercase letters followed by one or more lowercase letters
      .replace(/([A-Z]+)([A-Z])([a-z]+)/g, "$1 $2$3")
      // replaces underscore with blank
      .replace(/[_]/g, " ")
      // replaces multiple blanks with one blank
      .replace(/\s{2,}/, " ")
      .trim()
      .split(" ")
  );
}
