# Roadmap

This file provides an overview of the direction this project is heading. The following chronologically arranged milestones show which features are envisaged next. The earlier a feature is listed inside a milestone, the higher its priority.

## Milestones

### M1 - Basic functionalities and refinements

In this phase of the project the focus is on basic functionalities, which are necessary for a useful user experience. Its features are expected to be implemented in the short to mid term. The expected features are:

- Addition of suppression comments
- Refinement of the Law Of Demeter Check
- Update the pipeline, so that the extension gets bundled before publishing
- Refinement of the Error handling
- Revision of the handling of problem texts (factor out into resource files)
- Implement language depending switching of problem texts and help urls (and add a language setting variable to the config)
- Add integration Tests
- Add support for clean code rules about the avoidance of comments & object and data structure hybrids
- Add support for other simple clean code rules as described in the book clean code by Robert C. Martin
- Add support to automatically publish the newest version of the architecture documentation via the pipeline

### M2 - Extend language support

In this phase of the project the focus will be on extending the support for the most widely used programming languages. Its features are expected to be implemented in the mid to long term. The expected features are:

- Adding language support for C#
- Adding language support for JS/TS
- Adding language support for Golang
- Adding language support for Python
- Adding language support for C++

### M3 - Add language specific rules

In this phase of the project the focus will be on adding language specific clean code rules. Its features are expected to be implemented in the long term. The expected features are:

- Adding Java specific clean code rules
- Adding C# specific clean code rules
- Adding JS/TS specific clean code rules
- Adding Golang specific clean code rules
- Adding Python specific clean code rules
- Adding C++ specific clean code rules

## Not concretely planned

The following list of features would enrich the plugin, but are currently not in our focus and are therefore not planned until further notice:

- Extend the plugin support to other IDE's
- Adding support for not so common programming languages
- Integrate the plugin with other clean code tools, for example to evaluate clean code metrics centrally
